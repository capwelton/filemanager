<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';



require_once dirname(__FILE__) . '/functions.php';

bab_Functionality::includefile('PortletBackend');



$GLOBALS['Func_PortletBackend_FileManager_Categories'] = array(
    'portal_tools'	=> filemanager_translate('Portal tools')
);


/**
 * Crm Portlet backend
 */
class Func_PortletBackend_FileManager extends Func_PortletBackend
{


    public function getDescription()
    {
        return filemanager_translate('File Management');
    }



    public function select($category = null)
    {
        $addon = bab_getAddonInfosInstance('filemanager');
        if (!$addon || !$addon->isAccessValid()) {
            return array();
        }

        global $Func_PortletBackend_FileManager_Categories;

        if (empty($category) || in_array($category, $Func_PortletBackend_FileManager_Categories)) {
            return array(
                'FileManager' => $this->portlet_FileManager(),
            );
        }

        return array();
    }



    public function portlet_FileManager()
    {
        return new filemanager_PortletDefinition_FileManager();
    }


    /**
     * get a list of categories supported by the backend
     * @return Array
     */
    public function getCategories()
    {
        global $Func_PortletBackend_FileManager_Categories;

        return $Func_PortletBackend_FileManager_Categories;

    }

    /**
     * (non-PHPdoc)
     * @see Func_PortletBackend::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}







////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////


class filemanager_PortletDefinition_FileManager implements portlet_PortletDefinitionInterface
{
    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;

    /**
     * @param Func_Crm $crm
     */
    public function __construct()
    {
        $this->addon = bab_getAddonInfosInstance('filemanager');
    }


    public function getId()
    {
        return 'FileManager';
    }


    public function getName()
    {
        return filemanager_translate('File Manager');
    }


    public function getDescription()
    {
        return filemanager_translate('A simple two-panel file manager.');
    }


    public function getPortlet()
    {
        return new filemanager_Portlet_FileManager();
    }


    /**
     * @return array
     */
    public function getPreferenceFields()
    {
        return array(
            1 => array(
                'type' => 'bab_file',
                'label' => filemanager_translate('Public base folder'),
                'name' => 'baseFolder'
            ),
            2 => array(
                'type' => 'boolean',
                'label' => filemanager_translate('Use personal folder instead (enable will ignore public base folder)'),
                'name' => 'userFolder'
            ),
            3 => array(
                'type' => 'list',
                'name' => 'displayType',
                'label' => filemanager_translate('Display type'),
                'options' => array(
                    array(
                        'label' => filemanager_translate('Icons'),
                        'value' => 'icons'
                    ),
                    array(
                        'label' => filemanager_translate('Compact'),
                        'value' => 'compact'
                    ),
                    array(
                        'label' => filemanager_translate('Details'),
                        'value' => 'details'
                    ),
                    array(
                        'label' => filemanager_translate('Thumbnails'),
                        'value' => 'thumbnail'
                    ),
                    array(
                        'label' => filemanager_translate('Medium thumbnails'),
                        'value' => 'medium thumbnail'
                    ),
                    array(
                        'label' => filemanager_translate('Small thumbnails'),
                        'value' => 'small thumbnail'
                    ),
                )
            ),
            4 => array(
                'type' => 'boolean',
                'label' => filemanager_translate('Hide side panel'),
                'name' => 'hideDirectoryTree'
            ),
            5 => array(
                'type' => 'boolean',
                'label' => filemanager_translate('Hide toolbar'),
                'name' => 'hideDirectoryToolbar'
            ),
            6 => array(
                'type' => 'list',
                'name' => 'firstColumnName',
                'label' => filemanager_translate('Column 1'),
                'options' => array(
                    array(
                        'label' => filemanager_translate('Name'),
                        'value' => 'name'
                    ),
                    array(
                        'label' => filemanager_translate('Size'),
                        'value' => 'size'
                    ),
                    array(
                        'label' => filemanager_translate('Author'),
                        'value' => 'author'
                    ),
                    array(
                        'label' => filemanager_translate('Modified on'),
                        'value' => 'modifiedOn'
                    ),
                    array(
                        'label' => filemanager_translate('Configuration'),
                        'value' => 'configuration'
                    ),
                    array(
                        'label' => filemanager_translate('Hide this column'),
                        'value' => 'hide'
                    )
                ),
                'default' => 'name'
            ),
            7 => array(
                'type' => 'list',
                'name' => 'secondColumnName',
                'label' => filemanager_translate('Column 2'),
                'options' => array(
                    array(
                        'label' => filemanager_translate('Name'),
                        'value' => 'name'
                    ),
                    array(
                        'label' => filemanager_translate('Size'),
                        'value' => 'size'
                    ),
                    array(
                        'label' => filemanager_translate('Author'),
                        'value' => 'author'
                    ),
                    array(
                        'label' => filemanager_translate('Modified on'),
                        'value' => 'modifiedOn'
                    ),
                    array(
                        'label' => filemanager_translate('Configuration'),
                        'value' => 'configuration'
                    ),
                    array(
                        'label' => filemanager_translate('Hide this column'),
                        'value' => 'hide'
                    )
                ),
                'default' => 'size'
            ),
            8 => array(
                'type' => 'list',
                'name' => 'thirdColumnName',
                'label' => filemanager_translate('Column 3'),
                'options' => array(
                    array(
                        'label' => filemanager_translate('Name'),
                        'value' => 'name'
                    ),
                    array(
                        'label' => filemanager_translate('Size'),
                        'value' => 'size'
                    ),
                    array(
                        'label' => filemanager_translate('Author'),
                        'value' => 'author'
                    ),
                    array(
                        'label' => filemanager_translate('Modified on'),
                        'value' => 'modifiedOn'
                    ),
                    array(
                        'label' => filemanager_translate('Configuration'),
                        'value' => 'configuration'
                    ),
                    array(
                        'label' => filemanager_translate('Hide this column'),
                        'value' => 'hide'
                    )
                ),
                'default' => 'author'
            ),
            9 => array(
                'type' => 'list',
                'name' => 'fourthColumnName',
                'label' => filemanager_translate('Column 4'),
                'options' => array(
                    array(
                        'label' => filemanager_translate('Name'),
                        'value' => 'name'
                    ),
                    array(
                        'label' => filemanager_translate('Size'),
                        'value' => 'size'
                    ),
                    array(
                        'label' => filemanager_translate('Author'),
                        'value' => 'author'
                    ),
                    array(
                        'label' => filemanager_translate('Modified on'),
                        'value' => 'modifiedOn'
                    ),
                    array(
                        'label' => filemanager_translate('Configuration'),
                        'value' => 'configuration'
                    ),
                    array(
                        'label' => filemanager_translate('Hide this column'),
                        'value' => 'hide'
                    )
                ),
                'default' => 'modifiedOn'
            ),
            10 => array(
                'type' => 'list',
                'name' => 'fifthColumnName',
                'label' => filemanager_translate('Column 5'),
                'options' => array(
                    array(
                        'label' => filemanager_translate('Name'),
                        'value' => 'name'
                    ),
                    array(
                        'label' => filemanager_translate('Size'),
                        'value' => 'size'
                    ),
                    array(
                        'label' => filemanager_translate('Author'),
                        'value' => 'author'
                    ),
                    array(
                        'label' => filemanager_translate('Modified on'),
                        'value' => 'modifiedOn'
                    ),
                    array(
                        'label' => filemanager_translate('Configuration'),
                        'value' => 'configuration'
                    ),
                    array(
                        'label' => filemanager_translate('Hide this column'),
                        'value' => 'hide'
                    )
                ),
                'default' => 'configuration'
            ),
        );
    }

    /**
     * Returns the widget rich icon URL.
     * 128x128 ?
     *
     * @return string
     */
    public function getRichIcon()
    {
        return $this->addon->getImagesPath() . 'icon48.png';
    }


    /**
     * Returns the widget icon URL.
     * 16x16 ?
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->addon->getImagesPath() . 'icon48.png';
    }

    /**
     * Get thumbnail URL
     * max 120x60
     */
    public function getThumbnail()
    {
        return $this->addon->getImagesPath() . 'thumbnail.png';
    }

    public function getConfigurationActions()
    {
        return array();
    }
}


$W = bab_Widgets();
$W->Frame();

class filemanager_Portlet_FileManager extends widget_Frame implements portlet_PortletInterface
{

    private $id;

    private $configuration;

    private $baseFolder;

    private $displayType;

    private $hideDirectoryTree;

    private $hideDirectoryToolbar;

    private $userFolder;

    private $firstColumnName;

    private $secondColumnName;

    private $thirdColumnName;

    private $fourthColumnName;

    private $fifthColumnName;


    /**
     * {@inheritDoc}
     * @see Widget_Widget::getName()
     */
    public function getName()
    {
        return ''; //get_class($this);
    }

    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::getPortletDefinition()
     */
    public function getPortletDefinition()
    {
        return new filemanager_PortletDefinition_FileManager();
    }


    /**
     * @param string $key
     * @param mixed $defaultValue
     * @return mixed
     */
    private function getConfiguration($key, $defaultValue)
    {
        if (isset($this->configuration[$key])) {
            return $this->configuration[$key];
        }
        return $defaultValue;
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPreferences()
     */
    public function setPreferences(array $configuration)
    {
        $this->configuration = $configuration;

        $this->baseFolder = $this->getConfiguration('baseFolder', '');
        $this->userFolder = $this->getConfiguration('userFolder', false);
        $this->displayType = $this->getConfiguration('displayType', 'icons');
        $this->hideDirectoryTree = $this->getConfiguration('hideDirectoryTree', false);
        $this->hideDirectoryToolbar = $this->getConfiguration('hideDirectoryToolbar', false);
        $this->firstColumnName = $this->getConfiguration('firstColumnName', 'name');
        $this->secondColumnName = $this->getConfiguration('secondColumnName', 'size');
        $this->thirdColumnName = $this->getConfiguration('thirdColumnName', 'author');
        $this->fourthColumnName = $this->getConfiguration('fourthColumnName', 'modifiedOn');
        $this->fifthColumnName = $this->getConfiguration('fifthColumnName', 'configuration');
    }


    /**
     * {@inheritDoc}
     * @see Widget_Frame::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $App = filemanager_App();
        $addon = bab_getAddonInfosInstance('filemanager');
        if (!$addon->isAccessValid()) {
            bab_debug('filemanager addon is not accessible, portlet is displayed empty');
            return '';
        }


        filemanager_setConf($this->id . '_currentFolder', $this->baseFolder);
        filemanager_setConf($this->id . '_userFolder', $this->userFolder);
        if ($this->userFolder) {
            filemanager_setConf($this->id . '_currentFolder', 'U' . bab_getUserId());
        }
        filemanager_setConf($this->id . '_baseFolder', $this->baseFolder);
        filemanager_setConf($this->id . '_displayType', $this->displayType);
        filemanager_setConf($this->id . '_hideDirectoryTree', $this->hideDirectoryTree);
        filemanager_setConf($this->id . '_hideDirectoryToolbar', $this->hideDirectoryToolbar);

        filemanager_setConf($this->id . '_firstColumnName', $this->firstColumnName);
        filemanager_setConf($this->id . '_secondColumnName', $this->secondColumnName);
        filemanager_setConf($this->id . '_thirdColumnName', $this->thirdColumnName);
        filemanager_setConf($this->id . '_fourthColumnName', $this->fourthColumnName);
        filemanager_setConf($this->id . '_fifthColumnName', $this->fifthColumnName);

        $this->setId('filemanager_' . $this->id); // The widget item id.
        $this->setLayout($App->Controller()->File(false)->fileManager($this->id));

//         /* @var $I Func_Icons */
//         $I = bab_functionality::get('Icons');

//         /* @var $J Func_Jquery */
//         $J = bab_functionality::get('jquery');

        $filemanagerAddon = bab_getAddonInfosInstance('filemanager');

        $display = parent::display($canvas);
        $display .= $canvas->loadScript($this->getId(), $filemanagerAddon->getTemplatePath() . 'filemanager.jquery.js');
        $display .= $canvas->loadStyleSheet($filemanagerAddon->getStylePath() . 'filemanager.css');
//         $display .= $canvas->loadStyleSheet($GLOBALS['babInstallPath']. 'styles/' . $J->getStyleSheetUrl());
//         $display .= $canvas->loadStyleSheet($I->getCss());

        return $display;
    }


    public function setPreference($name, $value)
    {

    }

    public function setPortletId($id)
    {
        $this->id = $id;
    }
}
