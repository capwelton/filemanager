<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

include_once $GLOBALS['babInstallPath'] . 'utilit/mailincl.php';
require_once $GLOBALS['babAddonPhpPath'].'functions.php';


class filemanager_Mail
{
    public $mail;
    public $nbRecipientsByMail;

    public $lb;
    public $stack;
    public $debug;
    public $log;
    public $statusMessages;

    public function __construct()
    {
        $this->mail = bab_mail();

        if (!$this->mail) {
            //$GLOBALS['babBody']->msgerror = sprintf(filemanager_translate("The addon 'workspace' cannot send e-mails, the mailing configuration seems invalid"), $GLOBALS['babAddonName']);
            return false;
        }

        if ($GLOBALS['BAB_SESS_LOGGED']) {
            $this->mail->mailFrom($GLOBALS['BAB_SESS_EMAIL'], $GLOBALS['BAB_SESS_USER']);
        }

        $this->nbRecipientsByMail = 1;
        $this->lb = "\n";
        $this->stack = array();
        $this->debug = array();
        $this->statusMessages = array();
        $this->log = '';
    }

    static public function emailIsValid($email)
    {
        return (strpos($email, '@') !== false);
    }


    /**
     * Defines the data of the email to send.
     *
     * @param string	$subject
     * @param string	$message
     * @param string	$link
     * @param string	$linklabel
     * @param int		$from				User id of sender
     *
     * @return unknown
     */
    function setData($subject, $message, $link, $linklabel, $from = false)
    {
        if (!$this->mail)
            return false;

        if (false !== $from) {
            $this->mail->mailFrom(bab_getUserEmail($from), bab_getUserName($from));
        }

        $this->mail->mailSubject($subject);

        /* $link = $GLOBALS['babUrlScript'].'?tg=login&cmd=detect&referer='. urlencode($link); */

        $this->message_txt = $message;

        $this->message_html = '<div align="left">' . bab_toHtml($message, BAB_HTML_ALL) . '</div>';
        if (false !== $link) {
            $this->link = true;
            $this->link_txt = $link;
            $this->link_html = bab_toHtml($link);
            $this->linklabel_txt = $linklabel;
            $this->linklabel_html = bab_toHtml($linklabel);
        } else {
            $this->link = false;
        }

        $this->debug[] = $this->message_txt;
        $this->log .= 'message : '.$subject."\n";

        $html = bab_printTemplate($this, $GLOBALS['babAddonHtmlPath'].'email.html', 'html');
        $text = bab_printTemplate($this, $GLOBALS['babAddonHtmlPath'].'email.html', 'text');
        $this->mail->mailBody($html, 'text/html');
        $this->mail->mailAltBody($text);
    }


    function attachFile($fname, $realname, $type)
    {
        $this->mail->mailFileAttach($fname, $realname, $type);
    }


    /**
     * Defines the list of recipients of the email.
     *
     * @param array		$recipients
     * @param string	$type			'mailTo' or 'mailBcc'
     * @return bool
     */
    function mailDestArray($recipients, $type)
    {
        if (!$this->mail
            || !in_array($type, array('mailTo','mailBcc'))
            || !is_array($recipients)
            || count($recipients) == 0) {
            return false;
        }

        $keys = array_keys($recipients);
        foreach ($keys as $key) {
            if (!self::emailIsValid($recipients[$key])) {
                unset($recipients[$key]);
            }
        }

        if (isset($this->stack[$type])) {
            $this->stack[$type] = array_merge($this->stack[$type], $recipients);
        } else {
            $this->stack[$type] = $recipients;
        }

        $this->stack[$type] = array_unique($this->stack[$type]);

        $this->log .= "destinataires : ".implode(',', $this->stack['mailTo'])."\n";
        return true;
    }


    function mail_pop($type)
    {
        for ($i = 0; $i < $this->nbRecipientsByMail; $i++) {
            $mail = array_pop($this->stack[$type]);
            if (!$mail && $i == 0) {
                return false;
            }
            if (!empty($mail)) {
                $this->debug[] = 'Add '.$type.' : '.$mail;
                $this->mail->$type($mail);
            }
        }
        return true;
    }

    function get_gust_recipients()
    {
        $this->mail->clearAllRecipients();
        $out = false;
        $types = array_keys($this->stack);
        foreach ($types as $type) {
            if ($this->mail_pop($type)) {
                $out = true;
            }
        }
        return $out;
    }

    function send()
    {
        if (!$this->mail) {
            return false;
        }
        $result = true;
        while($this->get_gust_recipients()) {
            $retry = 0;
            while (true !== $this->mail->send() && $retry < 5) {
                $retry++;
            }

            $dest = $this->mail->mailTo[0];
            if ($retry < 5) {
                $this->debug[] = '<span style="color:green">MAIL SEND OK</span> (try : '.($retry + 1).'/5)';
                $this->statusMessages[] = '(' . $dest[0] . ')';
                $this->statusMessages[] = '=> Mailer OK: ' . $this->mail->ErrorInfo();
//				pe_log(PE_LOG_MAIL, $this->log);
            } else {
                $this->debug[] = '<span style="color:red">MAIL SEND ERROR</span>';
                $this->statusMessages[] = '(' . $dest[0] . ')';
                $this->statusMessages[] = '=> Mailer ERROR: ' . $this->mail->ErrorInfo();
                $result = false;
                $this->log .= "error : ".$this->mail->ErrorInfo()."\n";
//				pe_log(PE_LOG_ERROR, $this->log);
            }

//			echo(implode("\n",$this->debug));
            $this->debug = array();
        }

        $this->stack = array();
        return $result;
    }
}

?>