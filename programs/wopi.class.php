<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


class filemanager_Wopi
{
    /**
     * @param filemanager_File $file
     */
    public static function checkFileInfo(filemanager_File $file)
    {
        $App = filemanager_App();
        $fullPath = $file->getFullPath();

        $fileInfo = array(
            'BaseFileName' => $file->name,
            'OwnerId' => $file->getAuthorId(),
            'Size' => filesize($fullPath),
            'UserId' => $file->getAuthorId(),
            'ReadOnly' => false,
            'UserCanWrite' => true,
            'SHA256' => base64_encode(hash_file('sha256', $fullPath, true)),
            'Version' => $file->getVersion(),
            'SupportsUpdate' => false,
            'SupportsLocks' => true,
            'UserFriendlyName' => bab_getUserName(bab_getUserId(), true),
            'LastModifiedTime' => gmdate('c', bab_mktime($file->getModifiedDate())),
            'FileVersionUrl' => $App->Controller()->File()->history($file->id)
        );
        $jsonString = json_encode($fileInfo);
        header('Content-Type: application/json');
        echo $jsonString;
        exit;
    }

    /**
     * @param filemanager_File $file
     */
    public static function getFile(filemanager_File $file)
    {
        file_put_contents("php://stdout", __METHOD__ . " : " . $file->name . "\n");
        if (!$file->canDownload()) {
            header("HTTP/1.0 4 Not Found");
        }

        $fullPath = $file->getFullPath();

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $file->name);
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($fullPath));
        readfile($fullPath);
        exit;
    }


    /**
     *
     * @param filemanager_File $file
     * @param string $tmpPath
     */
    public static function putFile(filemanager_File $file, $tmpPath)
    {
        if (!$file->canBeUpdated()) {
            header("HTTP/1.0 4 Not Found");
        }

        require_once $GLOBALS['babInstallPath'] . 'utilit/fileincl.php';
        require_once $GLOBALS['babInstallPath'] . 'utilit/uploadincl.php';

        file_put_contents("php://stdout", "isFileUpdated: $tmpPath\n");
        $isFileUpdated = saveUpdateFile(
            $file->id,
            bab_fmFile::move($tmpPath),
            $file->name,
            '',
            '',
            'N',
            'Y',
            false,
            false,
            0
        );
        file_put_contents("php://stdout", "isFileUpdated: " . ($isFileUpdated ? 'true' : 'false') . "\n");
        file_put_contents("php://stdout", "isFileUpdated: " . bab_getBody()->msgerror. "\n");
        file_put_contents("php://stdout", "path: " . $file->getFullPath() . "\n");

        if (!$isFileUpdated) {
            header("HTTP/1.0 500 Internal Server Error");
            header('X-WOPI-ServerError: Could not update file.');
        } else {
            header("HTTP/1.0 200 OK");
        }
        exit;
    }

    public static function lock(filemanager_File $file)
    {
        if (!$file->canBeUpdated()) {
            header("HTTP/1.0 4 Not Found");
        }
        if (!$isFileUpdated) {
            header("HTTP/1.0 409 Conflict ");
            header('X-WOPI-Lock: ');
        } else {
            header("HTTP/1.0 200 OK");
        }
    }


    public static function unlock(filemanager_File $file)
    {
        if (!$file->canBeUpdated()) {
            header("HTTP/1.0 4 Not Found");
        }
        if (!$isFileUpdated) {
            header("HTTP/1.0 409 Conflict ");
            header('X-WOPI-Lock: ');
        } else {
            header("HTTP/1.0 200 OK");
        }
    }
}
