<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/filemanApi.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/pathUtil.class.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/uploadincl.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/page.class.php';
require_once dirname(__FILE__) . '/file.php';
require_once dirname(__FILE__) . '/file.ui.php';

$App = filemanager_App();
$App->includeController();


/**
 * This controller manages actions that can be performed on files.
 */
class filemanager_CtrlFile extends filemanager_Controller
{

    const DISPLAY_DETAILS = 'details';
    const DISPLAY_ICONS = 'icons';
    const DISPLAY_COMPACT = 'compact';
    const DISPLAY_THUMBNAIL = 'thumbnail';
//     const DISPLAY_MEDIUM_THUMBNAIL = 'medium thumbnail';
//     const DISPLAY_SMALL_THUMBNAIL = 'small thumbnail';


    const DISPLAY_TINY = 'tiny';
    const DISPLAY_SMALL = 'small';
    const DISPLAY_MEDIUM = 'medium';
    const DISPLAY_LARGE = 'large';

    /**
     *
     * @param string $portletId
     * @param string $search
     */
    public function fileSearch($portletId = null, $search = null)
    {
        if (empty($search['keywords'])) {
            filemanager_setConf($portletId . '_searchKeywords', null);
            return true;
        }

        $value = $search['keywords'];

        if (bab_isAjaxRequest()) {
            // Strings received from an ajax connection are always UTF-8 encoded.
            $value = bab_getStringAccordingToDataBase($value, bab_Charset::UTF_8);
        } elseif (bab_Charset::getIso() !== bab_Charset::UTF_8) {
            $value = iconv(bab_Charset::getIso(), bab_Charset::UTF_8, $value);
            $value = html_entity_decode($value, ENT_QUOTES, bab_Charset::UTF_8);
            $value = iconv(bab_Charset::UTF_8, bab_Charset::getIso() . '//TRANSLIT', $value);
        }

        filemanager_setConf($portletId . '_searchKeywords', $value);

        return true;
    }






    /**
     *
     * @param string $portletId
     * @throws Exception
     * @return Widget_VBoxLayout
     */
    public function fileManager($portletId = null)
    {
        require_once dirname(__FILE__) . '/file.ui.php';

        $path = filemanager_getConf($portletId . '_currentFolder', null);
        $userFolder = filemanager_getConf($portletId . '_userFolder', false);
        $displayType = filemanager_getConf($portletId . '_displayType', self::DISPLAY_ICONS);
        $hideDirectoryTree = filemanager_getConf($portletId . '_hideDirectoryTree', false);
        $hideDirectoryToolbar = filemanager_getConf($portletId . '_hideDirectoryToolbar', null);
        $searchKeywords = filemanager_getConf($portletId . '_searchKeywords', null);

        $columnConfNames = array('firstColumnName', 'secondColumnName', 'thirdColumnName', 'fourthColumnName', 'fifthColumnName');
        $columnNames = array();

        foreach ($columnConfNames as $columnConfName) {
            if (filemanager_getConf($portletId . '_' . $columnConfName)) {
                $columnNames[] = filemanager_getConf($portletId . '_' . $columnConfName);
            }
        }

        if (!$userFolder) {
            $path = filemanager_sanitizePath($path);
            $delegationId = filemanager_getDelegationFromPath($path);
            bab_setCurrentUserDelegation($delegationId);
        } else {
            $GLOBALS['babAutoAddFilesAuthorId'] = bab_getUserId();
        }


        $splitview = filemanager_folderSplitView($portletId, $path, $displayType, $searchKeywords, $hideDirectoryTree, $hideDirectoryToolbar, $userFolder, $columnNames);
        $splitview->setId('filemanager_port_' . $portletId);

        $splitview->addClass('filemanager');

        $splitview->setReloadAction($this->proxy()->fileManager($portletId));

        return $splitview;
    }







    /**
     * Displays the file manager.
     *
     * @return Widget_Page
     */
    public function displayFileManager($path = null, $root = null)
    {
         global $babBody;

         if (!isset($root)) {
             $root = $path;
         }

         $itemId = 'fm_' . str_replace('/', '_', $root);

         filemanager_setConf($itemId . '_currentFolder', $path);
         filemanager_setConf($itemId . '_baseFolder', $root);

         $W = bab_Widgets();

         $page = $W->BabPage();
//         $page->addItemMenu('files', filemanager_translate('File manager'), '');


         $page->addItem(
             $W->Frame()->addItem($this->fileManager($itemId))
                ->addClass('portlet-content')
         );

         $filemanagerAddon = bab_getAddonInfosInstance('filemanager');

         $babBody->addJavascriptFile($filemanagerAddon->getTemplatePath() . 'filemanager.jquery.js');
         $babBody->addStyleSheet($filemanagerAddon->getStylePath() . 'filemanager.css');

         return $page;
    }





    /**
     * Displays the file browser for the specified folder path.
     *
     * @param string $path		The path of the folder to browse. Eg. 'DG1/Folder/Subfolder'
     *
     * @return Widget_Page
     */
    public function browse($path = null)
    {
        return $this->displayFileManager($path);
    }





    /**
     * Displays (outputs) the specified file.
     *
     * @param string	$pathname
     * @param bool		$inline		true to have the file open inline or false to have it as an attachement (open outside the browser).
     * @return Widget_Action
     */
    public function displayFile($pathname, $inline = false, $user = false)
    {
        require_once dirname(__FILE__) . '/file.ui.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';

        $path = dirname($pathname);
        $pathElements = explode('/', $path);
        $path = implode('/', $pathElements);
        $filename = basename($pathname);

        if (!filemanager_canDownloadFileFromFolder($path, $user)) {
            bab_userIsloggedin() || bab_requireCredential(filemanager_translate('You should be logged in to see the content of this folder.'));
            throw new filemanager_AccessException();
        }

        $files = filemanager_listFolder($path, 0, $user);
        foreach ($files as $file) {
            if ($file->getFilename() == $filename) {
                $mimetype = bab_getFileMimeType($filename);
                $size = $file->getSize();
                if (strtolower(bab_browserAgent()) == 'msie') {
                    header('Cache-Control: public');
                }
                if (!empty($inline)) {
                    header('Content-Disposition: inline; filename="' . $filename . '"' . "\n");
                } else {
                    header('Content-Disposition: attachment; filename="' . $filename . '"' . "\n");
                }
                header('Content-Type: ' . $mimetype . "\n");
                header('Content-Length: ' . $size . "\n");
                readfile($file->getPathName());
                die;
            }
        }
    }
    
    /**
     * Displays (outputs) the specified file.
     *
     * @param int	$fileVerId Id of a filemanager_FileVersion record
     * @return Widget_Action
     */
    public function displayVersionFile($fileVerId)
    {
        require_once dirname(__FILE__) . '/file.ui.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/delegincl.php';
        
        $App = filemanager_App();
        $fileSet = $App->FileSet();
        $fileSet->fmPath();
        $fileVersionSet = $App->FileVersionSet();
        
        /*@var $fileVersion filemanager_FileVersion*/
        $fileVersion = $fileVersionSet->request($fileVersionSet->id->is($fileVerId));
        /*@var $file filemanager_File*/
        $file = $fileSet->request($fileSet->id->is($fileVersion->id_file));
        
        if (!filemanager_canDownloadFileFromFolder($file->fmPath, $file->isPersonal())) {
            bab_userIsloggedin() || bab_requireCredential(filemanager_translate('You should be logged in to see the content of this folder.'));
            throw new filemanager_AccessException();
        }
        
        $fileFullPath = $file->getFullPath(false);
        $fileDirectoryFullPath = dirname($fileFullPath);
        $versionDirectoryPath = $fileDirectoryFullPath.'/OVF';
        
        $versionFileName = $fileVersion->ver_major.','.$fileVersion->ver_minor.','.$file->name;
        
        $versionFileFullPath = $versionDirectoryPath.'/'.$versionFileName;
        if(!file_exists($versionFileFullPath)){
            throw new Exception(sprintf(filemanager_translate('The version %s of the file %s has not been found'), $fileVersion->getVersion(), $file->name));
        }
        
        
        $mimetype = bab_getFileMimeType($versionFileName);
        $size = filesize($versionFileFullPath);
        
        if (strtolower(bab_browserAgent()) == 'msie') {
            header('Cache-Control: public');
        }
        header('Content-Disposition: attachment; filename="' . $file->name . '"' . "\n");
        header('Content-Type: ' . $mimetype . "\n");
        header('Content-Length: ' . $size . "\n");
        readfile($versionFileFullPath);
        die;
    }





    /**
     * Creates the specified folder
     *
     * @param array 	$folder		Eg.: array('path' => 'DG1/Folder', 'name' => 'Subfolder')
     * @return Widget_Action
     */
    public function createFolder($folder = null)
    {
        if (!filemanager_canCreateFolderInFolder($folder['path'], $folder['user'])) {
            bab_userIsloggedin() || bab_requireCredential(filemanager_translate('You should be logged in to create folders.'));
            throw new filemanager_AccessException();
        }

        $folder['name'] = bab_getStringAccordingToDataBase($folder['name'], bab_charset::UTF_8);
        $folder['path'] = bab_getStringAccordingToDataBase($folder['path'], bab_charset::UTF_8);
        $pathname = $folder['path'] . '/' . bab_PathUtil::sanitizePathItem($folder['name']);
        $currentDirectoryPath = $folder['path'];

        if ($folder['user']) {
            if (mb_strlen(trim($folder['name'])) > 0) {
                $sUploadPath = BAB_FmFolderHelper::getUploadPath();

                if (!isStringSupportedByFileSystem($pathname)) {
                    throw new filemanager_AccessException(filemanager_translate('There was a problem when trying to create this folder.'));
                } else {
                    $sFullPathName = $sUploadPath . 'fileManager/users/' . $pathname;
                    BAB_FmFolderHelper::createDirectory($sFullPathName);
                }
            } else {
                throw new filemanager_AccessException(filemanager_translate('There was a problem when trying to create this folder.'));
            }
        } else {
            $currentDirectory = new bab_Directory();

            if (!$currentDirectory->createSubdirectory($pathname)) {
                throw new filemanager_AccessException(filemanager_translate('There was a problem when trying to create this folder.'));
            }
        }

        if (bab_isAjaxRequest()) {
            die;
        }

        filemanager_redirect($this->proxy()->browse($currentDirectoryPath));
    }



    /**
     * Displays the form to upload a file.
     *
     * @param string $path	The path of the folder where the file will be uploaded. Eg.: 'DG1/Folder/Subfolder'
     * @return bool
     */
    public function addFile($portletId, $path = '/', $userFolder = false)
    {
        require_once $GLOBALS['babInstallPath'] . '/utilit/uuid.php';
        $addon = bab_getAddonInfosInstance('filemanager');
        $tempPath = new bab_Path($addon->getUploadPath(), 'temp', $portletId);

        $W = bab_Widgets();
        $filePicker = $W->FilePicker();

        $documents = $filePicker->getFolderFiles($tempPath);
        
        if($userFolder){
            $fullDestPath = $GLOBALS['babUploadPath'] . '/fileManager/users/U' . bab_getUserId() . '/' . $path;
        }
        else{
            $fullDestPath = $GLOBALS['babUploadPath'] . '/fileManager/collectives/' . $path;
        }
        
        $uploadedFilesStatus = array();

        foreach ($documents as $document) {
            $destFilename = $document->toString();
            if(file_exists($fullDestPath.'/'.$destFilename)){
                $uploadedFilesStatus['pending'][] = array(
                    'uuid' => bab_uuid(),
                    'fileName' => $destFilename,
                    'destination' => $path,
                    'destFilename' => $destFilename,
                    'userFolder' => $userFolder,
                    'origin' => $document->getFilePath()->tostring()
                );
            }
            else{
                $this->uploadWithNewName($document->getFilePath()->toString(), $path, $destFilename, $userFolder);
                $uploadedFilesStatus['imported'][] = array(
                    'uuid' => bab_uuid(),
                    'fileName' => $destFilename
                );
            }
        }

        $_SESSION['filemanager_uploadedFilesStatus'] = $uploadedFilesStatus;
        
        return true;
    }



    /**
     *
     * @param string    $portletId
     * @param string    $path
     * @return boolean|Widget_Action
     */
    public function changeCurrentFolder($portletId, $path)
    {
        filemanager_setConf($portletId . '_currentFolder', $path);
        return true;
    }



    public function changeCurrentDisplayType($portletId, $displayType)
    {
        filemanager_setConf($portletId . '_displayType', $displayType);
        return true;
    }

    public function changeCurrentDisplayIconSize($portletId, $iconSize)
    {
        filemanager_setConf($portletId . '_displayIconSize', $iconSize);
        return true;
    }


    public function toggleDirectoryTreeVisibility($portletId)
    {
        filemanager_setConf($portletId . '_hideDirectoryTree', !filemanager_getConf($portletId . '_hideDirectoryTree'));
        return true;
    }



    public function toggleDirectoryToolbarVisibility($portletId)
    {
        filemanager_setConf($portletId . '_hideDirectoryToolbar', !filemanager_getConf($portletId . '_hideDirectoryToolbar'));
    	return true;
    }




    /**
     * Returns the error message corresponding to the upload error code.
     *
     * @param int $errorCode
     *
     * @return null|string
     */
    protected static function getUploadErrorMessage($errorCode)
    {
        $errorMessage = null;
        switch ($errorCode) {
            case UPLOAD_ERR_OK:
                break;

            case UPLOAD_ERR_INI_SIZE:
                $errorMessage = bab_translate('The uploaded file exceeds the upload_max_filesize directive.');
                break;

            case UPLOAD_ERR_FORM_SIZE:
                $errorMessage = bab_translate('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.');
                break;

            case UPLOAD_ERR_PARTIAL:
                $errorMessage = bab_translate('The uploaded file was only partially uploaded.');
                break;

            case UPLOAD_ERR_NO_FILE:
                $errorMessage = bab_translate('No file was uploaded.');
                break;

            case UPLOAD_ERR_NO_TMP_DIR: //  since PHP 4.3.10 and PHP 5.0.3
                $errorMessage = bab_translate('Missing a temporary folder.');
                break;

            case UPLOAD_ERR_CANT_WRITE: //  since php 5.1.0
                $errorMessage = bab_translate('Failed to write file to disk.');
                break;

            default :
                $errorMessage = bab_translate('Unknown File Error.');
                break;
        }
        return $errorMessage;
    }







    /**
     * Replace the file with the same name in the destination folder.
     *
     * @param string $sourceFilename Filename of the file to upload.
     * @param string $destPath       Path to the destination folder (starting at DGxx/)
     */
    public function uploadAndReplace($sourceFilename, $destPath)
    {
        global $babBody;

        $tmpdir = $GLOBALS['babUploadPath'] . '/tmp/' . session_id();

        $oFolderFileSet = new BAB_FolderFileSet();

        $oPathName = & $oFolderFileSet->aField['sPathName'];
        $oName = & $oFolderFileSet->aField['sName'];
        $idDgOwner = & $oFolderFileSet->aField['iIdDgOwner'];

        $pathElements = explode('/', $destPath);

        $basepath = implode('/', $pathElements) . '/';

        $delegationId = filemanager_getCurrentWorkspace();

        $oCriteria = $oPathName->in($basepath);
        $oCriteria = $oCriteria->_and($oName->in($sourceFilename));
        $oCriteria = $oCriteria->_and($idDgOwner->in($delegationId));
        $oFolderFile = $oFolderFileSet->get($oCriteria);
        if (is_null($oFolderFile)) {
            $babBody->addError(filemanager_translate('There was a problem when trying to upload this file.'));
            filemanager_redirect($this->proxy()->browse($destPath));
        }

        $isFileUpdated = saveUpdateFile(
            $oFolderFile->getId(),
            bab_fmFile::move($tmpdir . '/' . $sourceFilename),
            $sourceFilename,
            '',
            '',
            'N',
            'Y',
            false,
            false,
            0
        );
        if (!$isFileUpdated) {
            $babBody->addError(filemanager_translate('There was a problem when trying to upload this file.'));
            filemanager_redirect($this->proxy()->browse($destPath));
        }

        filemanager_redirect($this->proxy()->browse($destPath));
    }



    /**
     * Upload the file and rename it automatically until there is no file with the same
     * name in the destination folder.
     *
     * @param string $sourceFilename Filename of the file to upload.
     * @param string $destPath       Path to the destination folder (starting at DGxx/)
     * @param string $uuid           A uuid referencing an entry in the session that must be cleaned. This is used in the uploadStatus box when uploading a file
     */
    public function uploadWithNewName($sourceFilePath, $destPath, $destFilename, $userFolder = false, $uuid = null)
    {
        global $babBody;
        $success = false;
        $msg = '';
        $sourceFilePath = $rename = str_replace('\\', '/', $sourceFilePath);
        if ($userFolder) {
            $destPath = explode('/', $destPath);
            array_shift($destPath);
            $destPath = implode('/', $destPath);

            $fullDestPath = $GLOBALS['babUploadPath'] . '/fileManager/users/U' . bab_getUserId() . '/' . $destPath;

            $destFilenameElements = explode('.', $destFilename);

            if (count($destFilenameElements) > 1) {
                $extension = '.' . array_pop($destFilenameElements);
                $destFileNameStart = implode('.', $destFilenameElements);
            } else {
                $extension = '';
                $destFileNameStart = $destFilename;
            }

            $destFileNameStart = BAB_PathUtil::sanitizePathItem($destFileNameStart);

            $fullDestPathnameStart = $fullDestPath . '/' . $destFileNameStart;

            $renameAttempts = 1;
            $suffix = '';
            $hasBeenRenamed = false;
            while (file_exists($fullDestPathnameStart . $suffix . $extension)) {
                $suffix = '(' . $renameAttempts . ')';
                $hasBeenRenamed = true;
                $renameAttempts++;
            }

            $tmpdir = dirname($sourceFilePath);

            if (!rename($sourceFilePath, $tmpdir . '/' . $destFileNameStart . $suffix . $extension)) {
                $babBody->addError(filemanager_translate('There was a problem when trying to upload this file 1.'));
            } else {
                if (!filemanager_personalFileUpload($tmpdir . '/' . $destFileNameStart . $suffix . $extension, $destPath)) {
                    $babBody->addError(filemanager_translate('There was a problem when trying to upload this file 2.'));
                }
                else{
                    $msg = $hasBeenRenamed ? sprintf(filemanager_translate('The file has been renamed as %s'), $destFileNameStart.$suffix.$extension) : filemanager_translate('The file has been imported');
                    $babBody->addMessage($msg);
                    $success = true;
                }
            }
        } else {
            $fullDestPath = $GLOBALS['babUploadPath'] . '/fileManager/collectives/' . $destPath;

            $destFilenameElements = explode('.', $destFilename);

            if (count($destFilenameElements) > 1) {
                $extension = '.' . array_pop($destFilenameElements);
                $destFileNameStart = implode('.', $destFilenameElements);
            } else {
                $extension = '';
                $destFileNameStart = $destFilename;
            }

            $destFileNameStart = BAB_PathUtil::sanitizePathItem($destFileNameStart);

            $fullDestPathnameStart = $fullDestPath . '/' . $destFileNameStart;

            $renameAttempts = 1;
            $suffix = '';
            $hasBeenRenamed = false;
            while (file_exists($fullDestPathnameStart . $suffix . $extension)) {
                $suffix = '(' . $renameAttempts . ')';
                $hasBeenRenamed = true;
                $renameAttempts++;
            }

            $tmpdir = dirname($sourceFilePath);
            
            $rename = $tmpdir . '/' . $destFileNameStart . $suffix . $extension;
            $rename = str_replace('\\', '/', $rename);
            if (!rename($sourceFilePath, $rename)) {
                $msg = filemanager_translate('There was a problem when trying to upload this file.');
            } else {
                $currentDirectory = new bab_Directory();
                if (!$currentDirectory->importFile($tmpdir . '/' . $destFileNameStart . $suffix . $extension, $destPath)) {
                    $errors = $currentDirectory->getError();
                    $msg = implode('<br>', $errors);
                }
                else{
                    $msg = $hasBeenRenamed ? sprintf(filemanager_translate('The file has been renamed as %s'), $destFileNameStart.$suffix.$extension) : filemanager_translate('The file has been imported');
                    $success = true;
                }
            }
        }
        if(isset($uuid)){
            //Clean entry in session used in the uploadStatus box
            if(isset($_SESSION['filemanager_uploadedFilesStatus'])){
                $filemanager_uploadedFilesStatus = array();
                foreach ($_SESSION['filemanager_uploadedFilesStatus'] as $status => $uploadedFiles){
                    foreach ($uploadedFiles as $uploadedFile){
                        if($uploadedFile['uuid'] == $uuid){
                            if(!empty($msg)){
                                $uploadedFile['message'] = $msg;
                                $uploadedFile['isErrorMessage'] = !$success;
                            }
                            else{
                                unset($uploadedFile['message']);
                                unset($uploadedFile['isErrorMessage']);
                            }
                            $tmpStatus = $success ? 'imported' : $status;
                            $filemanager_uploadedFilesStatus[$tmpStatus][] = $uploadedFile;
                        }
                        else{
                            $filemanager_uploadedFilesStatus[$status][] = $uploadedFile;
                        }
                    }
                }
                $_SESSION['filemanager_uploadedFilesStatus'] = $filemanager_uploadedFilesStatus;
            }
        }
        return true;
    }





    /**
     *
     * @param string    $pathname
     * @param string    $newName
     * @param bool      $userFolder
     */
    public function renameFile($pathname = null, $newName = null, $userFolder = null)
    {
        $App = $this->App();
        if (!isset($pathname) || !isset($newName)) {
            die;
        }

        $pathname = bab_getStringAccordingToDataBase($pathname, bab_charset::UTF_8);

        $newName = bab_getStringAccordingToDataBase($newName, bab_charset::UTF_8);
        $newName = bab_PathUtil::sanitizePathItem($newName);

        $path = dirname($pathname);

        if (!filemanager_canRenameFileInFolder($path, $userFolder)) {
            $this->addError($App->translate('Access denied'));
            return true;
        }

        if ($userFolder) {
            $pathname = str_replace('U' . bab_getUserId() . '/', '', $pathname);
            $path = str_replace('U' . bab_getUserId() . '/', '', $path);
            filemanager_personalFileUpdate($pathname, $path . '/' . $newName);
        } else {

            $directory = new bab_Directory();

            if (!$directory->renameFile($pathname, $path . '/' . $newName)) {
                $errors = $directory->getError();
                foreach ($errors as $error) {
                    $this->addError($error);
                }
                return true;
            }
        }

        return true;
    }


    /**
     *
     * @param string    $pathname
     * @param string    $newName
     * @param bool      $userFolder
     */
    public function renameFolder($pathname = null, $newName = null, $userFolder = null)
    {
        $App = $this->App();
        if (!isset($pathname) || !isset($newName)) {
            die;
        }

        $pathname = bab_getStringAccordingToDataBase($pathname, bab_charset::UTF_8);

        $newName = bab_getStringAccordingToDataBase($newName, bab_charset::UTF_8);
        $newName = bab_PathUtil::sanitizePathItem($newName);

        $path = dirname($pathname);

        if (!filemanager_canRenameFileInFolder($path, $userFolder)) {
            $this->addError($App->translate('Access denied'));
            return true;
        }

        if ($userFolder) {
            $pathname = str_replace('U' . bab_getUserId() . '/', '', $pathname);
            $path = str_replace('U' . bab_getUserId() . '/', '', $path);
            filemanager_personalFolderUpdate($pathname, $path . '/' . $newName);
        } else {
            $directory = new bab_Directory();

            if (!$directory->renameSubDirectory($pathname, $path . '/' . $newName)) {
                $this->addError($App->translate('Access denied'));
                return true;
            }
        }

        return true;
    }




    /**
     * Move a file.
     *
     * @param string	$sourcePathname		The source file pathname. Eg.: 'DG1/My documents/text.odt'
     * @param string	$destPath			The destination folder path. Eg.: 'DG1/My documents/Backup/text.odt'
     * @return Widget_Action
     */
    public function moveFile($sourcePathname, $destPath, $userFolder = false)
    {
        $filename = basename($sourcePathname);
        $destPathname = $destPath . '/' . $filename;

        if (!filemanager_canMoveFile(dirname($sourcePathname), $destPath, $userFolder)) {
            bab_userIsloggedin() || bab_requireCredential(filemanager_translate('You should be logged in to move this file.'));
            throw new filemanager_AccessException();
        }

        if ($userFolder) {
            $sourcePathname = str_replace('U' . bab_getUserId() . '/', '', $sourcePathname);
            $destPathname = str_replace('U' . bab_getUserId() . '/', '', $destPathname);
            filemanager_personalFileUpdate($sourcePathname, $destPathname);
        } else {
            $directory = new bab_Directory();

            if (!$directory->renameFile($sourcePathname, $destPathname)) {

                throw new filemanager_AccessException(filemanager_translate('There was a problem when trying to move this file.' . "\n" . implode("\n", $directory->getError())));
            }
        }

        die;
    }



    /**
     * Move a folder.
     *
     * @param string	$sourcePathname		The source folder pathname. Eg.: 'DG1/My documents/Folder1/SubFolder'
     * @param string	$destPath			The destination folder path. Eg.: 'DG1/My documents/Folder2'
     * @return Widget_Action
     */
    public function moveFolder($sourcePathname, $destPath, $userFolder = false)
    {
//		$path = dirname($sourcePathname);
        $filename = basename($sourcePathname);
        $destPathname = $destPath . '/' . $filename;

        if (!filemanager_canMoveFolder(dirname($sourcePathname), $destPath, $userFolder)) {
            bab_userIsloggedin() || bab_requireCredential(filemanager_translate('You should be logged in to move this folder.'));
            throw new filemanager_AccessException();
        }



        if ($userFolder) {
            $sourcePathname = bab_getStringAccordingToDataBase($sourcePathname, bab_charset::UTF_8);
            $destPathname = bab_getStringAccordingToDataBase($destPathname, bab_charset::UTF_8);
            $sourcePathname = str_replace('U' . bab_getUserId() . '/', '', $sourcePathname);
            $destPathname = str_replace('U' . bab_getUserId() . '/', '', $destPathname);
            filemanager_personalFolderMove($sourcePathname, $destPathname);
        } else {

            $directory = new bab_Directory();

            if (!$directory->renameSubDirectory($sourcePathname, $destPathname)) {
                throw new filemanager_AccessException(filemanager_translate('There was a problem when trying to move this folder.'));
            }
        }

        die;
    }




    /**
     * @return Widget_Action
     */
    public function saveFolderProperties($properties)
    {
        global $babBody;

        $pathname = $properties['pathname'];
        $newName = bab_PathUtil::sanitizePathItem($properties['name']);
        $path = dirname($pathname);

        if (!filemanager_canRenameFileInFolder($path)) {
            bab_userIsloggedin() || bab_requireCredential(filemanager_translate('You should be logged in to edit this folder properties.'));
            throw new filemanager_AccessException();
        }

        $directory = new bab_Directory();

        if (!$directory->renameSubDirectory($pathname, $path . '/' . $newName)) {
            $babBody->addError(filemanager_translate('There was a problem when trying to rename this folder.'));
        }

        filemanager_redirect($this->proxy()->browse($path));
    }




    /**
     * @return Widget_Action
     */
    public function saveFileProperties($properties)
    {
        global $babBody;

        $pathname = $properties['pathname'];
        $newName = bab_PathUtil::sanitizePathItem($properties['name']);
        $path = dirname($pathname);

        if (!filemanager_canRenameFileInFolder($path)) {
            bab_userIsloggedin() || bab_requireCredential(filemanager_translate('You should be logged in to edit this file properties.'));
            throw new filemanager_AccessException();
        }

        $directory = new bab_Directory();

        if (!$directory->renameFile($pathname, $path . '/' . $newName)) {
            $babBody->addError(filemanager_translate('There was a problem when trying to rename this file.'));
        }

        filemanager_redirect($this->proxy()->browse($path));
    }




    /**
     * Delete the specified folder.
     *
     * @param string $pathname	The folder pathname. Eg.: 'DG1/My documents/Folder1/SubFolder'
     * @return Widget_Action
     */
    public function deleteFolder($pathname, $userFolder = false)
    {
        global $babBody;

        $path = dirname($pathname);

        if (!filemanager_canDeleteFolderInFolder($path, $userFolder)) {
            bab_userIsloggedin() || bab_requireCredential(filemanager_translate('You should be logged in to delete this folder.'));
            throw new filemanager_AccessException();
        }

        if ($userFolder) {
            $pathname = str_replace('U' . bab_getUserId() . '/', '', $pathname);
            filemanager_personalFolderDelete($pathname);
        } else {
            $directory = new bab_Directory();

            if (!$directory->deleteSubDirectory($pathname)) {
                $babBody->addError(filemanager_translate('There was a problem when trying to delete this folder.'));
            }
        }

        die;
    }




    /**
     * Moves the specified file to the trash.
     *
     * @param string $pathname	 The file pathname. Eg.: 'DG1/My documents/text.odt'
     * @param bool   $definitive false = move to trash, true = delete
     *
     * @return Widget_Action
     */
    public function deleteFile($pathname, $definitive = true, $userFolder = false)
    {
        global $babBody;
        
        $App = filemanager_App();
        $set = $App->FileSet();
        $set->fmPathname();
        
        $file = $set->request($set->fmPathname->is($pathname));
        $path = dirname($pathname);
        
        if (!filemanager_canDeleteFileInFolder($path, $userFolder)) {
            bab_userIsloggedin() || bab_requireCredential(filemanager_translate('You should be logged in to delete this file.'));
            throw new filemanager_AccessException();
        }
        
        if ($definitive) {
            if(!$file->deleteFile(true)){
                $babBody->addError(filemanager_translate('There was a problem when trying to delete this file'));
            }
            else{
                $babBody->addMessage(filemanager_translate('The file has been successfully deleted'));
            }
        }
        else {
            if (!filemanager_moveFileToTrash($file->getFmPathName(false), $userFolder)) {
                $babBody->addError(filemanager_translate('There was a problem when trying to put this file to the trash'));
            }
            else{
                $babBody->addMessage(filemanager_translate('The file has been successfully put into the trash'));
            }
        }
        return true;
    }
    
    public function restoreFile($fmFileId)
    {
        global $babBody;
        
        $App = filemanager_App();
        $set = $App->FileSet();
        /* @var $file filemanager_File */
        $file = $set->get($set->id->is($fmFileId));
        if(!$file){
            $babBody->addError(filemanager_translate('File not found'));
            return true;
        }
        if(!$file->isTrashed()){
            $babBody->addError(filemanager_translate('File is not trashed'));
            return true;
        }
        if($file->canBeRestored())
        {
            if($file->restore())
            {
                $babBody->addMessage(filemanager_translate('File has been restored to its original path'));
                return true;
            }
        }
        else{
            $babBody->addError(filemanager_translate('This file cannot be restored : a file with the same name already exists in the original path of this file'));
            return true;
        }
        $babBody->addError(filemanager_translate('An error occured while restoring the file'));
        return true;
    }

    public function downloadZipFolder($fullPath = '', $gr = 'Y')
    {
        global $babDB;

        $oFileManagerEnv =& getEnvObject();
        $folderInfos = new bab_FileInfo($fullPath);
        $sDirName = $folderInfos->getFilename();

        $W = bab_Widgets();
        //page is used to handling errors. If we have erros, we display the error message in the page, otherwise the dowload will start
        $page = $W->Page();
        $page->setLayout($W->FlowLayout());

        if(mb_strlen(trim($sDirName)) > 0 && ($oFileManagerEnv->userIsInRootFolder() || $oFileManagerEnv->userIsInCollectiveFolder() || $oFileManagerEnv->userIsInPersonnalFolder()))
        {
//             if($gr == 'Y'){
//                 require_once $GLOBALS['babInstallPath'].'utilit/filemanApi.php';
//                 $folderPath = realpath($oFileManagerEnv->getCollectiveRootFmPath() . $oFileManagerEnv->sRelativePath . $sDirName);
//             }elseif($gr == 'N'){
//                 $folderPath = realpath($oFileManagerEnv->getPersonalPath($GLOBALS['BAB_SESS_USERID']) . $oFileManagerEnv->sRelativePath . $sDirName);
//             }else{
//                 if($sDirName == bab_translate('Private folder')){
//                     $gr= 'N';
//                     $personalPath = new bab_Path($oFileManagerEnv->getPersonalPath($GLOBALS['BAB_SESS_USERID']) . $oFileManagerEnv->sRelativePath);
//                     if(!$personalPath->isDir()){
//                         $personalPath->createDir();
//                     }
//                     $folderPath = realpath($oFileManagerEnv->getPersonalPath($GLOBALS['BAB_SESS_USERID']) . $oFileManagerEnv->sRelativePath);
//                 }else{
//                     $gr = 'Y';
//                     require_once $GLOBALS['babInstallPath'].'utilit/filemanApi.php';
//                     $folderPath = realpath($oFileManagerEnv->getCollectiveRootFmPath() . $oFileManagerEnv->sRelativePath . $sDirName);
//                 }
//             }

            if(!$folderInfos->isDir())
            {
                $page->addItem($W->Title(bab_translate("Invalid directory")));
                return $page;
            }
            if(getDirSize($fullPath) > $GLOBALS['babMaxZipSize']){
                $page->addItem($W->Title(bab_translate("The ZIP file size exceed the limit configured for the file manager")));
                return $page;
            }

            $sourcePath = new bab_Path($fullPath);
            $destPath = new bab_Path($GLOBALS['babUploadPath'],'tmp',session_id());
            if($destPath->isDir()){
                $destPath->deleteDir();
            }

            $destPath->createDir();
            $destPath->push($sDirName.'.zip');

            $sql = "SELECT * FROM " . BAB_FILES_TBL . " WHERE confirmed = 'N' AND iIdDgOwner = '".bab_getCurrentUserDelegation()."'";
            $res = $babDB->db_query($sql);
            $notApproveFile = array();
            while($arr = $babDB->db_fetch_assoc($res)){
                $tmpPath = new bab_Path($oFileManagerEnv->getCollectiveRootFmPath(),$arr['path'],$arr['name']);
                $notApproveFile[] = realpath($tmpPath->tostring());
            }
            /* @var $Zip Func_Archive_Zip */
            $Zip = bab_functionality::get('Archive/Zip');
            $Zip->open($destPath->tostring());
            filemanager_zipFolderFile($sourcePath, $Zip, $sDirName, $notApproveFile, $gr);
            $Zip->close();
            if(is_file($destPath->tostring())){
                $fp = fopen($destPath->tostring(), 'rb');
                if ($fp)
                {
                    bab_setTimeLimit(3600);

                    $name = basename($destPath->getBasename());
                    $fsize = filesize($destPath->tostring());

                    if (mb_strtolower(bab_browserAgent()) == 'msie') {
                        header('Cache-Control: public');
                    }
                    header('Content-Disposition: attachment; filename="'.$name.'"'."\n");
                    header('Content-Type: application/zip'."\n");
                    header('Content-Length: '.$fsize."\n");
                    header('Content-transfert-encoding: binary'."\n");
                    ob_clean();
                    flush();
                    readfile($destPath->tostring());
                    fclose($fp);
                }
            }

            return;
        }
        else
        {
            $page->addItem($W->Title(bab_translate("Access denied")));
            return $page;
        }
    }

    public function unzipFile($pathname)
    {
//         $fileObject = new bab_FileInfo($fullPathName);

//         if(!$fileObject->isFile()){
//             //File does not exist
//             $this->addError(filemanager_translate("The file was not found on the server"));
//             return true;
//         }
//         if($fileObject->getExtension() != 'zip'){
//             //File is not a zip
//             $this->addError(filemanager_translate("The file is not a zip file"));
//             return true;
//         }

        $App = filemanager_App();
        $fileSet = $App->FileSet();
        $file = $fileSet->getByFmPathname($pathname);
        if($file->unzipInSameDirectory()){
            $this->addMessage(filemanager_translate('The file has been unziped'));
        }
        else{
            $this->addError(filemanager_translate('An error occured while unzipping the file'));
        }
        return true;
    }



// 	/**
// 	 * Removes the specified file from the trash and put it baxk to its original folder.
// 	 *
// 	 * @param string $pathname	The file pathname. Eg.: 'DG1/My documents/text.odt'
// 	 *
// 	 * @return Widget_Action
// 	 */
// 	public function undeleteFile($pathname)
// 	{
// 		global $babBody;

// 		$path = dirname($pathname);

// 		if (!filemanager_canDeleteFileInFolder($path)) {
// 			bab_userIsloggedin() || bab_requireCredential(filemanager_translate('You should be logged in to delete this file.'));
// 			throw new filemanager_AccessException();
// 		}

// 		if (!filemanager_removeFileFromTrash($pathname)) {
// 			$babBody->addError(filemanager_translate('There was a problem when trying to restore this file from the trash.'));
// 		}

// 		filemanager_redirect($this->proxy()->browse($path));
// 	}



    /**
     * Does nothing and return to the previous page.
     *
     * @return Widget_Action
     */
    public function cancel()
    {
        $controller = new filemanager_Controller();
        $action = filemanager_BreadCrumbs::last();
        $action = Widget_Action::fromUrl($action);
        $controller->execute($action);
//		filemanager_redirect(filemanager_BreadCrumbs::last());
    }
    
    /**
     * 
     * @return Widget_Label
     */
    public function uploadStatus()
    {
        $W = bab_Widgets();
        
        $App = filemanager_App();
        $set = $App->FileSet();
        $set->fmPath();
        
        $box = $W->VBoxItems();
        $box->setSizePolicy('uploadStatusBox');
        $box->addClass(Func_Icons::ICON_LEFT_16);
        $fileCtrl = filemanager_Controller()->File();
        $itemsList = array();
        
        if(isset($_SESSION['filemanager_uploadedFilesStatus'])){
            $uploadedFilesStatus = $_SESSION['filemanager_uploadedFilesStatus'];
            foreach($uploadedFilesStatus as $status => $uploadedFiles){
                foreach ($uploadedFiles as $uploadedFile){
                    $item = $W->FlowItems()->setSizePolicy('uploadedEntries');
                    $item->addClass('uploadedEntry');
                    switch ($status){
                        case 'imported':
                            $statusLabel = filemanager_translate('Imported');
                            $statusClass = 'uploadSuccess';
                            break;
                        case 'pending':
                            $statusLabel = filemanager_translate('Pending');
                            $statusClass = 'uploadPending';
                            break;
                        case 'canceled':
                            $statusLabel = filemanager_translate('Canceled');
                            $statusClass = 'uploadCanceled';
                            break;
                        case 'versioned':
                            $statusLabel = filemanager_translate('Versioned');
                            $statusClass = 'uploadVersioned';
                            break;
                        case 'error':
                            $statusLabel = filemanager_translate('Error');
                            $statusClass = 'uploadError';
                            break;
                        default:
                            $statusLabel = filemanager_translate('Unknown status');
                            $statusClass = 'uploadUnknown';
                            break;
                    }
                    $item->addItem(
                        $W->FlowItems(
                            $W->FlowItems(
                                $W->Label(bab_nbsp())->setSizePolicy('uploadDot '.$statusClass),
                                $W->Label($uploadedFile['fileName'])
                            )->setSizePolicy('uploadedFileName'),
                            $W->FlowItems(
                                $W->Label($statusLabel),
                                $W->Label('')->setAjaxAction($fileCtrl->cleanUploadStatus($uploadedFile['uuid']))->setSizePolicy('uploadedFileClean')
                                    ->addClass('icon', Func_Icons::ACTIONS_DIALOG_CANCEL)
                            )->setSizePolicy('uploadedFileImportStatus')
                        )->setSizePolicy('uploadedInfos')
                    );
                    if($status == 'pending'){
                        $path = rtrim($uploadedFile['destination']).'/';
                        $file = $set->get($set->fmPath->is($path)->_AND_($set->name->is($uploadedFile['fileName'])));
                        $canCreateNewVersion = true;
                        $isFolderVersioned = true;
                        
                        $fileFolder = $file->getFolderInfos();
                        if(!$fileFolder->isVersioned()){
                            //The folder is not versioned
                            $canCreateNewVersion = false;
                            $isFolderVersioned = false;
                        }
                        if($file->isLocked()){
                            if(!$file->canBeUnlockedByUser() || !$file->canBeUpdated()){
                                //The user can't unlock or update the file
                                $canCreateNewVersion = false;
                            }
                        }
                        if($canCreateNewVersion){
                            //Versioning is active and the user can create a new version
                            $newVersionItem = $W->Link(
                                filemanager_translate('Create a new version'),
                                $fileCtrl->uploadWithNewVersion($uploadedFile['origin'], $uploadedFile['destination'], $uploadedFile['fileName'], $uploadedFile['userFolder'], $uploadedFile['uuid'])
                            )->setAjaxAction()
                            ->setSizePolicy('uploadedAction');
                        }
                        else if($isFolderVersioned){
                            //Versioning is active for the folder, but the file is locked
                            $userName = bab_getUserName($file->lockedBy());
                            $msg = $userName ? sprintf(filemanager_translate('This file has been locked by %s'), $userName) : filemanager_translate('This file is locked');
                            $newVersionItem = $W->Label($msg);
                        }
                        else{
                            //Versioning is not active for the folder
                            $newVersionItem = null;
                        }
                        $item->addItems(
                            $W->Label(filemanager_translate('This file already exists'))->setSizePolicy('uploadedMessage'),
                            $W->ListItems(
                                $newVersionItem,
                                $W->Link(
                                    filemanager_translate('Rename this file'),
                                    $fileCtrl->uploadWithNewName($uploadedFile['origin'], $uploadedFile['destination'], $uploadedFile['destFilename'], $uploadedFile['userFolder'], $uploadedFile['uuid'])
                                )->setAjaxAction()
                                ->setSizePolicy('uploadedAction'),
                                $W->Link(
                                    filemanager_translate('Cancel the upload'),
                                    $fileCtrl->cancelUpload($uploadedFile['origin'], $uploadedFile['uuid'])
                                )->setAjaxAction()
                                ->setSizePolicy('uploadedAction')
                            )->setSizePolicy('uploadedActions')
                        );
                    }
                    if(isset($uploadedFile['message']) && !empty($uploadedFile['message'])){
                        $item->addItem(
                            $W->Label($uploadedFile['message'])->setSizePolicy('uploadedMessage')
                        );
                        $class = 'uploadedSuccess';
                        if(isset($uploadedFile['isErrorMessage']) && $uploadedFile['isErrorMessage']){
                            $class = 'uploadedError';
                        }
                        $item->addClass($class);
                    }
                    $itemsList[] = $item;
                }
            }
        }
        $itemsAdded = count($itemsList);
        if($itemsAdded > 0){
            $box->addItem(
                $W->FlowItems(
                    $W->Label(sprintf(filemanager_translate('%d file imported', '%d files imported', $itemsAdded), $itemsAdded))->setSizePolicy('uploadStatusTitle'),
                    $W->Label('')->setAjaxAction($fileCtrl->cleanUploadStatus())->setSizePolicy('uploadStatusClean')
                    ->addClass('icon', Func_Icons::ACTIONS_DIALOG_CANCEL)
                )->setSizePolicy('uploadStatusTitleBox')
            );
            $content = $W->FlowItems()->setSizePolicy('uploadStatusContent');
            foreach ($itemsList as $index => $item)
            {
                if($index + 1 < $itemsAdded){
                    $item->addClass('uploadSeparator');
                }
                $content->addItem($item);
            }
            $box->addItem($content);
        }
//         $_SESSION['filemanager_uploadedFilesStatus'] = null;
        return $box;
    }
    
    public function cleanUploadStatus($uuid = null)
    {
        if(!isset($uuid)){
            $_SESSION['filemanager_uploadedFilesStatus'] = null;
        }
        else{
            //Clean entry in session used in the uploadStatus box
            if(isset($_SESSION['filemanager_uploadedFilesStatus'])){
                $filemanager_uploadedFilesStatus = array();
                foreach ($_SESSION['filemanager_uploadedFilesStatus'] as $status => $uploadedFiles){
                    foreach ($uploadedFiles as $uploadedFile){
                        if($uploadedFile['uuid'] != $uuid){
                            $filemanager_uploadedFilesStatus[$status][] = $uploadedFile;
                        }
                    }
                }
                $_SESSION['filemanager_uploadedFilesStatus'] = $filemanager_uploadedFilesStatus;
            }
        }
        return true;
    }
    
    public function cancelUpload($origin, $uuid = null)
    {
        $success = unlink($origin);
        if(isset($uuid)){
            //Clean entry in session used in the uploadStatus box
            if(isset($_SESSION['filemanager_uploadedFilesStatus'])){
                $filemanager_uploadedFilesStatus = array();
                foreach ($_SESSION['filemanager_uploadedFilesStatus'] as $status => $uploadedFiles){
                    foreach ($uploadedFiles as $uploadedFile){
                        if($uploadedFile['uuid'] == $uuid){
                            $tmpStatus = $success ? 'canceled' : $status;
                            $filemanager_uploadedFilesStatus[$tmpStatus][] = $uploadedFile;
                        }
                        else{
                            $filemanager_uploadedFilesStatus[$status][] = $uploadedFile;
                        }
                    }
                }
                $_SESSION['filemanager_uploadedFilesStatus'] = $filemanager_uploadedFilesStatus;
            }
        }
        return true;
    }
    
    public function uploadWithNewVersion($sourceFilePath, $destPath, $filename, $userFolder = false, $uuid = null)
    {
        $success = false;
        $msg = '';
        
        $App = filemanager_App();
        $set = $App->FileSet();
        $set->fmPath();
        $set->fmPathname();
        
        $cleanedDestPath = rtrim($destPath, '/').'/';
        /*@var $fmFile filemanager_File*/
        $fmFile = $set->get(
            $set->fmPath->is($cleanedDestPath)
            ->_AND_($set->name->is($filename))
            ->_AND_($set->state->in(''))
        );
        
        if(!$fmFile){
            $msg = filemanager_translate('There was a problem when trying to upload this file');
        }
        else{
            $fileFolder = $fmFile->getFolderInfos();
            if(!$fmFile->canBeUpdated()){
                $msg = filemanager_translate('You don\'t have the necessary rights to update this file');
            }
            elseif(!$fileFolder){
                //Folder parent not found
                $msg = filemanager_translate('There was a problem when trying to upload this file');
            }
            elseif(!$fileFolder->isVersioned()){
                //Folder parent is not versioned
                $msg = filemanager_translate('The parent folder does not accept versioned files');
            }
            elseif($fmFile->isLocked() && !$fmFile->canBeUnlockedByUser()){
                //File is locked and user cannot create a new version
                $userName = bab_getUserName($fmFile->lockedBy());
                $msg = $userName ? sprintf(filemanager_translate('This file has been locked by %s'), $userName) : filemanager_translate('This file is locked');
            }
            else{
                //User can create a new version of the file
                fm_lockFile($fmFile->id, '');
                $success = fm_commitFile($fmFile->id, '', 'N', bab_fmFile::move($sourceFilePath));
                if($success){
                    //Refresh fmFile info as it has now a new version
                    $fmFile = $set->request($set->id->is($fmFile->id));
                    $newVersion = $fmFile->getVersion();
                    $msg = sprintf(filemanager_translate('Version %s'), $newVersion);
                }
                else{
                    $msg = filemanager_translate('An error occured while trying to version the file');
                }
            }
        }
        if(isset($uuid)){
            //Clean entry in session used in the uploadStatus box
            if(isset($_SESSION['filemanager_uploadedFilesStatus'])){
                $filemanager_uploadedFilesStatus = array();
                foreach ($_SESSION['filemanager_uploadedFilesStatus'] as $status => $uploadedFiles){
                    foreach ($uploadedFiles as $uploadedFile){
                        if($uploadedFile['uuid'] == $uuid){
                            $uploadedFile['message'] = $msg;
                            $uploadedFile['isErrorMessage'] = !$success;
                            $tmpStatus = $success ? 'versioned' : 'error';
                            $filemanager_uploadedFilesStatus[$tmpStatus][] = $uploadedFile;
                        }
                        else{
                            $filemanager_uploadedFilesStatus[$status][] = $uploadedFile;
                        }
                    }
                }
                $_SESSION['filemanager_uploadedFilesStatus'] = $filemanager_uploadedFilesStatus;
            }
        }
        return true;
    }
    
    public function history($fileId = null)
    {
        $W = bab_Widgets();
        $page = $W->BabPage();
        
        $App = filemanager_App();
        
        $set = $App->FileSet();
        $file = $set->request($set->id->is($fileId));
        
        $versionTableView = filemanager_fileHistory($file);
        $page->addItem($versionTableView);
        
        $page->setTitle(sprintf(filemanager_translate('Versions history of file %s'), $file->name));
        
        
        
        return $page;
    }
    
    public function lockFile($fmFileId)
    {
        global $babBody;
        
        $App = filemanager_App();
        $set = $App->FileSet();
        $set->fmPath();
        
        $file = $set->request($set->id->is($fmFileId));
        if(!filemanager_canUpdateFileInFolder($file->fmPath, $file->isPersonal())){
            $babBody->addError(filemanager_translate('You don\'t have the necessary rights to lock this file'));
            return true;
        }
        if($file->isLocked()){
            $babBody->addError(filemanager_translate('This file is already locked'));
            return true;
        }
        fm_lockFile($file->id, '');
        $babBody->addMessage(sprintf(filemanager_translate('The file %s has been locked'), $file->name));
        return true;
    }
    
    public function unlockFile($fmFileId)
    {
        global $babBody;
        
        $App = filemanager_App();
        $set = $App->FileSet();
        $set->fmPath();
        
        $file = $set->request($set->id->is($fmFileId));
        if(!filemanager_canUpdateFileInFolder($file->fmPath, $file->isPersonal())){
            $babBody->addError(filemanager_translate('You don\'t have the necessary rights to unlock this file'));
            return true;
        }
        if(!$file->isLocked()){
            $babBody->addError(filemanager_translate('This file is already unlocked'));
            return true;
        }
        if(!$file->canBeUnlockedByUser()){
            $babBody->addError(filemanager_translate('This file has been locked by someone else'));
            return true;
        }
        fm_unlockFile($file->id, '');
        $babBody->addMessage(sprintf(filemanager_translate('The file %s has been unlocked'), $file->name));
        return true;
    }
    
    public function changeDescription($file = null, $changeDesc = array())
    {
        global $babBody;
        
        if (!isset($file) || !isset($changeDesc['description'])) {
            die;
        }
        $App = filemanager_App();
        $set = $App->FileSet();
        
        /* @var $file filemanager_File */
        $file = $set->request($set->id->is($file));
        
        $description = bab_getStringAccordingToDataBase($changeDesc['description'], bab_charset::UTF_8);

        if (!$file->canBeUpdated()) {
            $this->addError($App->translate('Access denied'));
            return true;
        }
        
        $file->description = $description;
        if(!$file->save()){
            $babBody->addError(filemanager_translate('There was an error while trying to change the description'));
        }
        else{
            $babBody->addMessage(filemanager_translate('The description has been changed'));
        }
        
        return true;
    }
    
    public function changeTags($file = null, $changeTags = array())
    {
        global $babBody;
        if (!isset($file)) {
            $babBody->addError(filemanager_translate('File not found'));
            return true;
        }
        $App = filemanager_App();
        $set = $App->FileSet();
        $tags = isset($changeTags['tags']) ? $changeTags['tags'] : array();
        
        /* @var $file filemanager_File */
        $file = $set->request($set->id->is($file));
        
        if (!$file->canBeUpdated()) {
            $this->addError($App->translate('Access denied'));
            return true;
        }

        $file->setThesaurusTags($tags);
        $babBody->addMessage(filemanager_translate('The tags have been updated'));
        return true;
    }
}
;