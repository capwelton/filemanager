<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/file.class.php';
require_once $GLOBALS['babInstallPath'] . '/utilit/fileincl.php';
require_once $GLOBALS['babInstallPath'] . '/utilit/filemanApi.php';



/**
 *
 * @param string    $path	Path of the file (Example : c:\images\img.jpg)
 * @param int       $width
 * @param int       $height
 * @return null|false|string
 */
function filemanager_fileIconUrl($path, $width, $height)
{
    /* @var $T Func_Thumbnailer */
    $T = bab_functionality::get('Thumbnailer');

    $imageUrl = null;
    if ($T) {
        $T->setSourceFile($path);
        $imageUrl = $T->getThumbnail($width, $height);
    }

    return $imageUrl;
}


/**
 * Checks whether the current user can list the content of the specified folder.
 *
 * @param string $folderPath		The folder path
 * @return bool
 */
function filemanager_canBrowseFolder($folderPath, $userFolder)
{
    if ($userFolder) {
        return bab_userHavePersonnalStorage();
    } else {
        $directory = new bab_Directory();
        // This is a hack to do some initialization. Do not remove.
        $directory->canManage($folderPath);

        $pathName = $directory->getPathName();

        $canBrowse = canBrowse($pathName);
        return $canBrowse;
    }
}

/**
 * Checks whether the current user can upload a file in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @param bool $userFolder				True if it is a user folder
 * @return bool
 */
function filemanager_canUploadFileInFolder($folderPath, $userFolder)
{
    if ($userFolder) {
        return bab_userHavePersonnalStorage();
    } else {
        $directory = new bab_Directory();
        // This is a hack to do some initialization. Do not remove.
        $directory->canManage($folderPath);
        $pathName = $directory->getPathName();
        $canUpload = canUpload($pathName);
        return $canUpload;
    }
}



/**
 * Checks whether the current user can download a file from the specified folder.
 *
 * @param string $folderPath			The folder path
 * @param bool $userFolder				True if it is a user folder
 * @return bool
 */
function filemanager_canDownloadFileFromFolder($folderPath, $userFolder)
{
    if ($userFolder) {
        return bab_userHavePersonnalStorage();
    } else {
        $directory = new bab_Directory();
        // This is a hack to do some initialization. Do not remove.
        $directory->canManage($folderPath);
        $pathName = $directory->getPathName();
        $canUpload = canDownload($pathName);
        return $canUpload;
    }
}



/**
 * Checks whether the current user can delete a file in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @param bool $userFolder				True if it is a user folder
 * @return bool
 */
function filemanager_canDeleteFileInFolder($folderPath, $userFolder)
{

    if ($userFolder) {
        return bab_userHavePersonnalStorage();
    } else {
        $directory = new bab_Directory();
        // This is a hack to do some initialization. Do not remove.
        $directory->canManage($folderPath);
        $pathName = $directory->getPathName();
        $canDelete = canDelFile($pathName);
        return $canDelete;
    }
}


/**
 * Checks whether the current user can delete a (sub)folder in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @param bool $userFolder				True if it is a user folder
 * @return bool
 */
function filemanager_canDeleteFolderInFolder($folderPath, $userFolder)
{

    if ($userFolder) {
        return bab_userHavePersonnalStorage();
    } else {
        $directory = new bab_Directory();
        // This is a hack to do some initialization. Do not remove.
        $directory->canManage($folderPath);
        $pathName = $directory->getPathName();
        $canDelete = canDelFile($pathName);
        return $canDelete;
    }
}


/**
 * Checks whether the current user can create a (sub)folder in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @param bool $userFolder				True if it is a user folder
 * @return bool
 */
function filemanager_canCreateFolderInFolder($folderPath, $userFolder)
{
    if ($userFolder) {
        return bab_userHavePersonnalStorage();
    } else {
        $directory = new bab_Directory();
        // This is a hack to do some initialization. Do not remove.
        $directory->canManage($folderPath);
        $pathName = $directory->getPathName();

        $canDelete = canCreateFolder($pathName);
        return $canDelete;
    }
}


/**
 * Checks whether the current user can rename a file in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @param bool $userFolder				True if it is a user folder
 * @return bool
 */
function filemanager_canRenameFileInFolder($folderPath, $userFolder)
{
    if ($userFolder) {
        return bab_userHavePersonnalStorage();
    } else {
        $directory = new bab_Directory();
        // This is a hack to do some initialization. Do not remove.
        $directory->canManage($folderPath);
        $pathName = $directory->getPathName();

        $canRename = canUpdate($pathName);
        return $canRename;
    }
}

/**
 * Checks whether the current user can update the specified folder.
 *
 * @param string $folderPath			The folder path
 * @param bool $userFolder				True if it is a user folder
 * @return bool
 */
function filemanager_canUpdateFolder($folderPath, $userFolder)
{
    if ($userFolder) {
        return bab_userHavePersonnalStorage();
    } else {
        $directory = new bab_Directory();
        // This is a hack to do some initialization. Do not remove.
        $directory->canManage($folderPath);
        $pathName = $directory->getPathName();
        
        $canRename = canUpdate($pathName);
        return $canRename;
    }
}



/**
 * Checks whether the current user can replace a file in the specified folder.
 *
 * @param string $folderPath			The folder path
 * @param bool $userFolder				True if it is a user folder
 * @return bool
 */
function filemanager_canUpdateFileInFolder($folderPath, $userFolder)
{
    if ($userFolder) {
        return bab_userHavePersonnalStorage();
    } else {
        $directory = new bab_Directory();
        // This is a hack to do some initialization. Do not remove.
        $directory->canManage($folderPath);
        $pathName = $directory->getPathName();

        $canUpdate = canUpdate($pathName);
        return $canUpdate;
    }
}



/**
 * Checks whether the current user can move a file between the specified folders.
 *
 * @param string $sourceFolderPath			The source folder path
 * @param string $destFolderPath			The dest folder path
 * @return bool
 */
function filemanager_canMoveFile($sourceFolderPath, $destFolderPath, $userFolder)
{
    if ($userFolder) {
        $source = str_replace('U'.bab_getUserId(), '', $sourceFolderPath);
        $dest = str_replace('U'.bab_getUserId(), '', $destFolderPath);
        if($source == $destFolderPath || $dest == $sourceFolderPath){
            return false;
        } else {
            return bab_userHavePersonnalStorage();
        }
    } else {
        $canMove = filemanager_canDeleteFileInFolder($sourceFolderPath, $userFolder)
                    && filemanager_canUploadFileInFolder($destFolderPath, $userFolder);
        return $canMove;
    }
}



/**
 * Checks whether the current user can move a file between the specified folders.
 *
 * @param string $sourceFolderPath			The source folder path
 * @param string $destFolderPath			The dest folder path
 * @return bool
 */
function filemanager_canMoveFolder($sourceFolderPath, $destFolderPath, $userFolder)
{
    if ($userFolder) {
        $source = str_replace('U'.bab_getUserId(), '', $sourceFolderPath);
        $dest = str_replace('U'.bab_getUserId(), '', $destFolderPath);
        if($source == $sourceFolderPath || $dest == $destFolderPath){
            return false;
        } else {
            return bab_userHavePersonnalStorage();
        }
    } else {
        $canMove = filemanager_canDeleteFolderInFolder($sourceFolderPath, $userFolder)
            && filemanager_canCreateFolderInFolder($destFolderPath, $userFolder);
        return $canMove;
    }
}





/**
 * Checks whether the current user can view the trash folder.
 *
 * @return bool
 */
function filemanager_canViewTrash()
{
    return false;//filemanager_userIsWorkspaceAdministrator();
}


/**
 * Lists the content of a folder in the addon's upload folder.
 *
 * @param string	$relativePath	The relative path of the folder from the addon's upload path.
 * @return array
 */
function filemanager_listFolder2($relativePath)
{
    $dirIterator = new DirectoryIterator($GLOBALS['babAddonUpload'] . $relativePath);
    $_files = array();

    foreach ($dirIterator as $file) {
        $filename = $file->getFilename();
        if ($filename === '.' || $filename === '..') {
            continue;
        }

        $_files[$file->getPathname()] = $file->getFileInfo();
    }

    return $_files;
}



function filemanager_compareFilesByNameDirFirst(bab_FileInfo $file1, bab_FileInfo $file2)
{
    if ($file1->isDir() && !$file2->isDir()) {
        return -1;
    }
    if (!$file1->isDir() && $file2->isDir()) {
        return +1;
    }
    return strcasecmp($file1->getFilename(), $file2->getFilename());
}





/**
 * Lists the content of a folder in the addon's upload folder.
 *
 * @param string $relativePath	 The relative path of the folder from the addon's upload path.
 * @param int    $iExcludeFilter bab_DirectoryFilter::DIR, bab_DirectoryFilter::HIDDEN, bab_DirectoryFilter::DOT, bab_DirectoryFilter::FILE
 *
 * @return bab_FileInfo[]
 */
function filemanager_listFolder($relativePath, $iExcludeFilter = 0, $user = false)
{
    $_files = array();
    $App = filemanager_App();
    $set = $App->FileSet();
    $set->fmPathname();
    if ($user) {
        $oFileManagerEnv = & getEnvObject();
        $sFullPathname = (string) BAB_FmFolderHelper::getUploadPath() . BAB_FileManagerEnv::relativeFmPersonalPath . $relativePath.'/';

        if (is_dir(realpath($sFullPathname))) {
            $oDir = dir($sFullPathname);
            while (false !== ($sEntry = $oDir->read())) {
                // Skip pointers
                if($sEntry == '.' || $sEntry == '..' || $sEntry == BAB_FVERSION_FOLDER) {
                    continue;
                }

                $sFullPathNameEntry	= $sFullPathname . $oFileManagerEnv->sRelativePath . $sEntry;
                $sFullrelativePath = $relativePath.$sEntry;

                if (is_dir($sFullPathname . $sEntry)) {
                   // $sRelativePath		= $oFileManagerEnv->sRelativePath . $sEntry . '/';
                    //$sFullrelativePath = $relativePath.$sEntry.'/';
                    $bInClipBoard	= false;//(bool) array_key_exists($sFullPathName, $this->aCuttedDir);

                    if (false === $bInClipBoard) {
                        $_files[$sFullrelativePath.'/'] = new filemanager_UserFileInfo($sFullPathNameEntry.'/');
                    }

                } else {
                    $_files[$sFullrelativePath] = new filemanager_UserFileInfo($sFullPathNameEntry);
                }
            }
            $oDir->close();
        }

    } else {
        $oBabDir = new bab_Directory();
        try {
            $sPathName = $relativePath;
            $oIterator = $oBabDir->getEntries($sPathName, $iExcludeFilter);
            if ($oIterator instanceof bab_CollectiveDirIterator) {
                /* @var $oItem bab_FileInfo */
                foreach ($oIterator as $oItem) {
                    $fmPathName = $oItem->getFmPathname();
                    $fmFile = $set->get($set->fmPathname->in($fmPathName, 'TR'.$fmPathName)->_AND_($set->state->contains('D')));
                    if($fmFile){
                        continue;
                    }
                    $_files[$fmPathName] = $oItem;
                }
            }
        } catch (Exception $ex) {
            bab_debug($ex->getMessage());
        }
    }

    uasort($_files, 'filemanager_compareFilesByNameDirFirst');

    return $_files;
}



/**
 *
 * @return BAB_FolderFile[]
 */
function filemanager_listTrash($path)
{
    $App = filemanager_App();
    $set = $App->FileSet();
    
    $isPersonal = filemanager_File::isPersonalPath($path);
    $owner = $isPersonal ? filemanager_File::getPathOwnerId($path) : filemanager_File::getDelegationId($path);
    
    $conditions = array();
    $conditions[] = $set->state->contains('D');
    $conditions[] = $isPersonal ? $set->bgroup->is('N') : $set->bgroup->is('Y');
    $conditions[] = $isPersonal ? $set->id_owner->is($owner) : $set->iIdDgOwner->is($owner);
    
    $files = array();
    $trashedFiles = $set->select($set->all($conditions))->orderAsc($set->name);
    foreach ($trashedFiles as $trashedFile){
        /* @var $trashedFile filemanager_File */
        $files[$trashedFile->getFmPathName()] = $trashedFile->getFileInfo();
    }
    
    return $files;
}




// Prior to PHP5.1 RecursiveIteratorIterator::SELF_FIRST did not exist
if (!defined('RIT_SELF_FIRST')) {
    define('RIT_SELF_FIRST', RecursiveIteratorIterator::SELF_FIRST);
}



/**
 * Recursively lists the sub-folders of a folder in the addon's upload folder.
 *
 * @param string	$relativePath	The relative path of the folder from the addon's upload path.
 * @return array
 */
function filemanager_listFolderRecursive($path, $user = false)
{
    $files = filemanager_listFolder($path, bab_DirectoryFilter::FILE, $user);

    foreach ($files as $file) {
        $filename = $file->getFilename();
        if ($filename === '.' || $filename === '..' || !$file->isDir()) {
            continue;
        }
        $files += filemanager_listFolderRecursive($file->getFmPathname(), $user);
    }

    return $files;
}



/**
 * Renames a file before moving it to the trash.
 *
 * @param string $pathname The pathname of the file to move to trash eg. 'DGxx/RootFolderName/SubFolder/Filename.ext'.
 * @return string The new pathname or false
 */
function filemanager_renameFilenameToTrash($pathname)
{
    $directory = new bab_Directory();

    $dirname = dirname($pathname);
    $basename = basename($pathname);

    $newPathname = $dirname . '/.' . $basename;
    if (!$directory->renameFile($pathname, $newPathname)) {
        return false;
    }

    return $newPathname;
}



/**
 * Renames a file before removing it from the trash.
 *
 * @param string $pathname The pathname of the file to remove from trash eg. 'DGxx/RootFolderName/SubFolder/.Filename.ext'.
 * @return string|bool The new pathname or false
 */
function filemanager_renameFilenameFromTrash($pathname)
{
    $directory = new bab_Directory();

    $dirname = dirname($pathname);
    $basename = basename($pathname);

    $newPathname = $dirname . '/' . substr($basename, 1);
    if (!$directory->renameFile($pathname, $newPathname)) {
        return false;
    }

    return $newPathname;
}



/**
 * Moves the specified file to the trash.
 *
 * @param string $path The path of the file to move to trash eg. 'DGxx/RootFolderName/SubFolder/Filename.ext'.
 * @return boolean
 */
function filemanager_moveFileToTrash($path, $userFolder = false)
{
    if ($userFolder) {
        $oFolderFileSet = new BAB_FolderFileSet();

        $oState = $oFolderFileSet->aField['sState'];
        $oPathName = $oFolderFileSet->aField['sPathName'];
        $oName = $oFolderFileSet->aField['sName'];
        $idDgOwner = $oFolderFileSet->aField['iIdDgOwner'];
        $oGroup = $oFolderFileSet->aField['sGroup'];
        $oIdOwner = $oFolderFileSet->aField['iIdOwner'];

        $pathElements = explode('/', $path);

        array_shift($pathElements);
        $filename = array_pop($pathElements);
        $basepath = implode('/', $pathElements) . '/';


        $oCriteria = $oState->in('');
        $oCriteria = $oCriteria->_and($oPathName->in($basepath));
        $oCriteria = $oCriteria->_and($oName->in($filename));
        $oCriteria = $oCriteria->_and($idDgOwner->in(0));
        $oCriteria = $oCriteria->_and($oGroup->in('N'));
        $oCriteria = $oCriteria->_and($oIdOwner->in(bab_getUserId()));

        $oFolderFile = $oFolderFileSet->get($oCriteria);
        if (is_null($oFolderFile)) {
            return false;
        }
        $oFolderFile->setState('D');
        $oFolderFile->save();
        return true;
    } else {
        $delegationId = filemanager_File::getDelegationId($path);

        // We rename the file before moving it to the trash.
        $path = filemanager_renameFilenameToTrash($path);
        if ($path === false) {
            return false;
        }

        $oFolderFileSet = new BAB_FolderFileSet();

        $oState =& $oFolderFileSet->aField['sState'];
        $oPathName =& $oFolderFileSet->aField['sPathName'];
        $oName =& $oFolderFileSet->aField['sName'];
        $idDgOwner =& $oFolderFileSet->aField['iIdDgOwner'];

        $pathElements = explode('/', $path);

        array_shift($pathElements);
        $filename = array_pop($pathElements);
        $basepath = implode('/', $pathElements) . '/';


        $oCriteria = $oState->in('');
        $oCriteria = $oCriteria->_and($oPathName->in($basepath));
        $oCriteria = $oCriteria->_and($oName->in($filename));
        $oCriteria = $oCriteria->_and($idDgOwner->in($delegationId));

        $oFolderFile = $oFolderFileSet->get($oCriteria);
        if (is_null($oFolderFile)) {
            return false;
        }
        $oFolderFile->setState('D');
        $oFolderFile->save();
        return true;
    }
}




/**
 * Removes the specified file from the trash ans put it back to its origianl folder.
 *
 * @param string $path The path of the file to move to trash eg. 'DGxx/RootFolderName/SubFolder/Filename.ext'.
 * @return boolean
 */
function filemanager_removeFileFromTrash($path)
{
    // We rename the file before removing it from the trash.
    $path = filemanager_renameFilenameFromTrash($path);
    if ($path === false) {
        return false;
    }

    $delegationId = filemanager_getCurrentWorkspace();

    $oFolderFileSet = new BAB_FolderFileSet();

    $oState =& $oFolderFileSet->aField['sState'];
    $oPathName =& $oFolderFileSet->aField['sPathName'];
    $oName =& $oFolderFileSet->aField['sName'];
    $idDgOwner =& $oFolderFileSet->aField['iIdDgOwner'];

    $pathElements = explode('/', $path);

    array_shift($pathElements);
    $filename = array_pop($pathElements);
    $basepath = implode('/', $pathElements) . '/';


    $oCriteria = $oState->in('D');
    $oCriteria = $oCriteria->_and($oPathName->in($basepath));
    $oCriteria = $oCriteria->_and($oName->in($filename));
    $oCriteria = $oCriteria->_and($idDgOwner->in($delegationId));

    $oFolderFile = $oFolderFileSet->get($oCriteria);
    if (is_null($oFolderFile)) {
        return false;
    }
    $oFolderFile->setState('');
    $oFolderFile->save();
    return true;
}

define('FILEMANAGER_TRASH_FILENAME_PREFIX', '.');

function filemanager_convertTrashFilename($filename)
{
    return FILEMANAGER_TRASH_FILENAME_PREFIX . $filename;
}


function filemanager_revertTrashFilename($filename)
{
    if (substr($filename, 0, strlen(FILEMANAGER_TRASH_FILENAME_PREFIX)) === FILEMANAGER_TRASH_FILENAME_PREFIX) {
        $filename = substr($filename, strlen(FILEMANAGER_TRASH_FILENAME_PREFIX));
    }
    return $filename;
}



function  filemanager_personalFileUpload($sFullSrcFileName, $sPathName)
{
    require_once $GLOBALS['babInstallPath'].'utilit/uploadincl.php';

    if(!bab_userHavePersonnalStorage())
    {
        return false;
    }

    if($sPathName != ''){
        $sPathName = str_replace('U'.bab_getUserId().'/', '', $sPathName);
        $sPathName.= '/';
    }

    $oFileHandler = new bab_fileHandler(BAB_FILEHANDLER_MOVE, $sFullSrcFileName);
    if(!($oFileHandler instanceof bab_fileHandler))
    {
        //bab_translate("Error: Unable to instance bab_fileHandler");
        return false;
    }

    $sFileName		= basename($sFullSrcFileName);
    $sFullPathName	= $GLOBALS['babUploadPath'] . '/fileManager/users/U'.bab_getUserId().'/' .  $sPathName . $sFileName;
    if(false === $oFileHandler->import($sFullPathName))
    {
        //bab_translate("Error: Unable to import the file");
        return false;
    }
    //*/

    //$this->displayInfo();

    require_once $GLOBALS['babInstallPath'].'utilit/indexincl.php';
    $iIndexStatus = bab_indexOnLoadFiles(array($sFullPathName), 'bab_files');

    $oFolderFile = bab_getInstance('BAB_FolderFile');
    $oFolderFile->setName($sFileName);
    $oFolderFile->setPathName($sPathName);

    $oFolderFile->setOwnerId(bab_getUserId());
    $oFolderFile->setGroup('N');
    $oFolderFile->setCreationDate(date("Y-m-d H:i:s"));
    $oFolderFile->setAuthorId(bab_getUserId());
    $oFolderFile->setModifiedDate(date("Y-m-d H:i:s"));
    $oFolderFile->setModifierId(bab_getUserId());
    $oFolderFile->setConfirmed('Y');

    $oFolderFile->setDescription('');
    $oFolderFile->setLinkId(0);
    $oFolderFile->setReadOnly('N');
    $oFolderFile->setState('');
    $oFolderFile->setHits(0);
    $oFolderFile->setFlowApprobationInstanceId(0);
    $oFolderFile->setFolderFileVersionId(0);
    $oFolderFile->setMajorVer(1);
    $oFolderFile->setMinorVer(0);
    $oFolderFile->setCommentVer('');
    $oFolderFile->setStatusIndex($iIndexStatus);
    $oFolderFile->setDelegationOwnerId(0);
    $oFolderFile->setSize(filesize($sFullPathName));

    if(false === $oFolderFile->save())
    {
        //bab_translate("Error: Unable to create the file in the database");
        unlink($sFullPathName);
        return false;
    }

    $iIdFile = $oFolderFile->getId();
    $oFolderFile->setId(null); //bab_getInstance

    $oFolderFileLog = new BAB_FolderFileLog();
    $oFolderFileLog->setIdFile($iIdFile);
    $oFolderFileLog->setCreationDate(date("Y-m-d H:i:s"));
    $oFolderFileLog->setAuthorId($GLOBALS['BAB_SESS_USERID']);
    $oFolderFileLog->setAction(BAB_FACTION_INITIAL_UPLOAD);
    $oFolderFileLog->setComment(bab_translate("Initial upload"));
    $oFolderFileLog->setVersion('1.0');
    $oFolderFileLog->save();

    /*if(BAB_INDEX_STATUS_INDEXED === $iIndexStatus)
    {
        $obj = new bab_indexObject('bab_files');
        $obj->setIdObjectFile($sFullPathName, $iIdFile, $oFmFolder->getId());
    }*/

    return true;
}


function filemanager_personalFileUpdate($path, $newpath)
{
    getEnvObject();
    $oFolderFileSet	= bab_getInstance('BAB_FolderFileSet');
    $oName			= $oFolderFileSet->aField['sName'];
    $oPathName		= $oFolderFileSet->aField['sPathName'];
    $oGroup			= $oFolderFileSet->aField['sGroup'];
    $oIdOwner		= $oFolderFileSet->aField['iIdOwner'];
    $oIdDgOwner		= $oFolderFileSet->aField['iIdDgOwner'];

    $pathArray = explode('/', $path);
    $name = array_pop($pathArray);
    $pathSrc = implode('/',$pathArray);
    $pathSrc.= '/';
    if(substr($pathSrc, 0,1) == '/'){
        $pathSrc = substr($pathSrc, 1);
    }

    $newpathArray = explode('/', $newpath);
    $newname = array_pop($newpathArray);
    $pathTrg = implode('/',$newpathArray);
    $pathTrg.= '/';
    if(substr($pathTrg, 0,1) == '/'){
        $pathTrg = substr($pathTrg, 1);
    }

    $oCriteria		= $oName->in($name);
    $oCriteria		= $oCriteria->_and($oPathName->in($pathSrc));
    $oCriteria		= $oCriteria->_and($oGroup->in('N'));
    $oCriteria		= $oCriteria->_and($oIdOwner->in(bab_getUserId()));
    $oCriteria		= $oCriteria->_and($oIdDgOwner->in(0));

    //bab_debug($oFolderFileSet->getSelectQuery($oCriteria));

    $oFolderFile = $oFolderFileSet->get($oCriteria);
    if(!($oFolderFile instanceof BAB_FolderFile))
    {
        //bab_translate("Error: cannot get the file from the database");
        return false;
    }

    $sTrgName			= $newname;
    $sFullSrcPathName	= (string) BAB_FmFolderHelper::getUploadPath() . BAB_FileManagerEnv::relativeFmPersonalPath . 'U'.bab_getUserId().'/' .$path;
    $sFullTrgPathName	= (string) BAB_FmFolderHelper::getUploadPath() . BAB_FileManagerEnv::relativeFmPersonalPath . 'U'.bab_getUserId().'/' .$newpath;

    if(rename($sFullSrcPathName, $sFullTrgPathName))
    {
        $oFolderFile->setName($sTrgName);
        $oFolderFile->setPathName($pathTrg);
        $oFolderFile->save();

        return true;
    }
    return false;
}



function filemanager_personalFolderUpdate($folder, $newfolder)
{
    getEnvObject();
    $sRootFmPath = (string) BAB_FmFolderHelper::getUploadPath() . BAB_FileManagerEnv::relativeFmPersonalPath . 'U'.bab_getUserId().'/';

    $pathArray = explode('/', $folder);
    $newpathArray = explode('/', $newfolder);

    $OldfolderName = array_pop($pathArray);
    $NewfolderName = array_pop($newpathArray);

    $path = implode('/', $pathArray);

    $sOldFullPathname = (string) $sRootFmPath . $folder.'/';
    $sNewFullPathname = (string) $sRootFmPath . $newfolder.'/';

    $bFolderRenamed	= ($sOldFullPathname !== $sNewFullPathname) ? true : false;

    if($bFolderRenamed)
    {
        if(BAB_FmFolderHelper::renameDirectory($sRootFmPath, $path, $OldfolderName, $NewfolderName))
        {
            BAB_FolderFileSet::renameFolder($folder.'/', $NewfolderName, 'N');
            BAB_FmFolderCliboardSet::rename($path, $OldfolderName, $NewfolderName, 'N');
        }
    }
}





function  filemanager_personalFileDelete($file)
{
    getEnvObject();
    $oFolderFileSet	= bab_getInstance('BAB_FolderFileSet');
    $oName			= $oFolderFileSet->aField['sName'];
    $oPathName		= $oFolderFileSet->aField['sPathName'];
    $oGroup			= $oFolderFileSet->aField['sGroup'];
    $oIdOwner		= $oFolderFileSet->aField['iIdOwner'];
    $oIdDgOwner		= $oFolderFileSet->aField['iIdDgOwner'];

    $pathArray = explode('/', $file);
    $name = array_pop($pathArray);
    array_shift($pathArray);

    $path = implode('/', $pathArray);
    if($path && substr($path,-1) != '/'){
        $path.= '/';
    }

    $oCriteria		= $oName->in($name);
    $oCriteria		= $oCriteria->_and($oPathName->in($path));
    $oCriteria		= $oCriteria->_and($oGroup->in('N'));
    $oCriteria		= $oCriteria->_and($oIdOwner->in(bab_getUserId()));
    $oCriteria		= $oCriteria->_and($oIdDgOwner->in(0));

    $oFolderFile = $oFolderFileSet->get($oCriteria);
    if (!($oFolderFile instanceof BAB_FolderFile)) {
        //bab_translate("Error: cannot get the file from the database");
        return false;
    }

    $oFolderFileSet->remove($oCriteria);
    return true;
}



function filemanager_personalFolderDelete($folder)
{
    global $babDB, $babBody;

    $pathArray = explode('/', $folder);
    $name = array_pop($pathArray);
    $path = implode('/', $pathArray);

    getEnvObject();
    $sRootFmPath = (string) BAB_FmFolderHelper::getUploadPath() . BAB_FileManagerEnv::relativeFmPersonalPath . 'U'.bab_getUserId().'/';

    $sFullPathName = $sRootFmPath . $folder;

    if (!file_exists($sFullPathName)) {
        $babBody->msgerror = sprintf(bab_translate("The folder %s does not exists"), $folder);
        return false;
    }

    $oFolderFileSet = new BAB_FolderFileSet();
    $oPathName =& $oFolderFileSet->aField['sPathName'];
    $oIdOwner =& $oFolderFileSet->aField['iIdOwner'];
    $oGroup =& $oFolderFileSet->aField['sGroup'];
    $oFolderFileSet->remove(
        $oPathName->like($babDB->db_escape_like($folder) . '%')
        ->_and($oIdOwner->in(bab_getUserId()))
        ->_and($oGroup->in('N'))
    );

    $oFmFolderSet = new BAB_FmFolderSet();
    $oFmFolderSet->removeDir($sFullPathName);

    $oFmFolderCliboardSet = new BAB_FmFolderCliboardSet();
    $oFmFolderCliboardSet->deleteFolder($name, $path, 'N');

    return true;
}



/**
 *
 * @param string $source
 * @param string $dest
 * @return boolean
 */
function filemanager_helperRenameDirectory($source, $dest)
{
    return is_dir($source) && is_writable($source) && !is_dir($dest) && rename($source, $dest);
}



function filemanager_personalFolderMove($source, $dest)
{
    getEnvObject();
    $sRootFmPath = (string) BAB_FmFolderHelper::getUploadPath() . BAB_FileManagerEnv::relativeFmPersonalPath . 'U'.bab_getUserId().'/';

    $oldFullPathname = (string) $sRootFmPath . $source.'/';
    $newFullPathname = (string) $sRootFmPath . $dest.'/';

    if ($oldFullPathname == $newFullPathname) {
        return;
    }

    if (filemanager_helperRenameDirectory($oldFullPathname, $newFullPathname)) {
        BAB_FolderFileSet::move($source, $dest, 'N');
        $clipboardSet = new BAB_FmFolderCliboardSet();
        $clipboardSet->move($source, $dest, 'N');
    }
}

/**
 * Returns an array of all thesaurus tags in bab_tags
 * @return array
 */
function filemanager_getThesaurusTags()
{
    global $babDB;
    $query = 'SELECT * FROM '.BAB_TAGS_TBL;
    $result = $babDB->db_query($query);
    $tags = array();
    while($arr = $babDB->db_fetch_assoc($result)){
        $tags[] = $arr;
    }
    return $tags;
}

/**
 * If found, return the thesarus tag name associated to a thesaurus id, null otherwise
 * @param int $id The id of a tag
 * @return string | NULL
 */
function filemanager_getThesaurusTagNameById($id)
{
    global $babDB;
    $query = 'SELECT tag_name FROM '.BAB_TAGS_TBL.' WHERE id='.$babDB->quote($id);
    $result = $babDB->db_query($query);
    $res = $babDB->db_fetch_array($result);
    $tag = isset($res['tag_name']) ? $res['tag_name'] : null;
    return $tag;
}